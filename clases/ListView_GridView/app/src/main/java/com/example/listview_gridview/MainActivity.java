package com.example.listview_gridview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    private Button btnListText;
    private Button btnGridText;
    private Button btnListClass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Hook
        btnListText = findViewById(R.id.btnListText);
        btnGridText = findViewById(R.id.btnGridText);
        btnListClass = findViewById(R.id.btnListClass);

        btnListText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ListviewText.class);
                startActivity(intent);
            }
        });

    btnGridText.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(MainActivity.this, GridviewText.class);
            startActivity(intent);
        }
    });

        btnListClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ListviewClass.class);
                startActivity(intent);
            }
        });


    }
}