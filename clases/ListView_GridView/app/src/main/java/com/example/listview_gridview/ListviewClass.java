package com.example.listview_gridview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


import java.util.Arrays;
import java.util.List;

public class ListviewClass extends AppCompatActivity {

    private ListView listviewClass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview_class);

        //Hook
        listviewClass = findViewById(R.id.listviewClass);

        Item item1 = new Item(R.drawable.foto1, "Foto1", "Subtitle foto1", "2€");
        Item item2 = new Item(R.drawable.foto2, "Foto2", "Subtitle foto2", "1€");
        Item item3 = new Item(R.drawable.foto3, "Foto3", "Subtitle foto3", "3€");
        Item item4 = new Item(R.drawable.foto4, "Foto4", "Subtitle foto4", "4€");
        Item item5 = new Item(R.drawable.foto5, "Foto5", "Subtitle foto5", "2€");

        List<Item> items = Arrays.asList(item1, item2, item3, item4, item5);
        CustomAdapter customApdapter = new CustomAdapter(this, items);
        listviewClass.setAdapter(customApdapter);

    }

    private class CustomAdapter  extends BaseAdapter {

        private Context context;
        private List<Item> items; //data source of the list adapter

        public CustomAdapter(Context context, List<Item> items) {
            this.context = context;
            this.items = items;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.row_data, null);
            TextView itemTitle = view.findViewById(R.id.txtItemTitle);
            TextView itemSubtitle = view.findViewById(R.id.txtItemSubtilte);
            TextView itemPrice = view.findViewById(R.id.txtItemPrice);
            ImageView itemImage = view.findViewById(R.id.imgItem);

            itemTitle.setText(items.get(position).getTitle());
            itemSubtitle.setText(items.get(position).getSubtitle());
            itemPrice.setText(items.get(position).getPrice());
            itemImage.setImageResource(items.get(position).getImage());

            return view;
        }
    }
}