package com.example.listview_gridview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ListviewText extends AppCompatActivity {

    public  static String EXTRA_TEXT_DETAIL = "com.example.listview_gridview.EXTRA_TEXT_DETAIL";
    private ListView listviewText;

    List<String> list = new ArrayList<>();
    ArrayAdapter<String> dataAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview_text);

        listviewText = findViewById(R.id.listviewText);

        list.add("Texto 1");
        list.add("Texto 2");
        list.add("Texto 3");
        list.add("Texto 4");
        list.add("Texto 5");
        list.add("Texto 6");
        list.add("Texto 7");
        list.add("Texto 8");
        list.add("Texto 9");
        list.add("Texto 10");
        list.add("Texto 11");
        list.add("Texto 13");
        list.add("Texto 13");
        list.add("Texto 14");
        list.add("Texto 15");
        list.add("Texto 16");
        list.add("Texto 17");
        list.add("Texto 18");
        list.add("Texto 19");
        list.add("Texto 20");

        dataAdapter = new ArrayAdapter<String>(
                this,
                R.layout.support_simple_spinner_dropdown_item,
                list
        );
        listviewText.setAdapter(dataAdapter);
        listviewText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Toast.makeText(ListviewText.this, "click: " + position, Toast.LENGTH_SHORT).show();
            }
        });
        listviewText.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
                Toast.makeText(ListviewText.this, "Long click", Toast.LENGTH_SHORT).show();
                return true;
            }
        });



    }
}