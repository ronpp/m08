package com.example.menutoilbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Hook
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {


        switch (item.getItemId()){
            case R.id.itemShop:
                Toast.makeText(MainActivity.this, "Shop", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.itemSetting:
                Toast.makeText(MainActivity.this, "Setting", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.itemProfile:
                Toast.makeText(MainActivity.this, "Profile", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.itemCatalog:
                Toast.makeText(MainActivity.this, "Catalog", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.itemShop1:
                Toast.makeText(MainActivity.this, "SubItem shop 1", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.itemShop2:
                Toast.makeText(MainActivity.this, "SubItem shop 2", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.itemShop3:
                Toast.makeText(MainActivity.this, "SubItem shop 3", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }

    }
}