package com.example.radiobutton;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

     private RadioGroup radioGroup1;
     private RadioGroup radioGroup2;
     private RadioButton radio1;
     private RadioButton radio2;
     private RadioButton radio3;
     private RadioButton radio4;
     private RadioButton radio5;
     private RadioButton radio6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // Hook
        radio1 = findViewById(R.id.radio1);
        radio2 = findViewById(R.id.radio2);
        radio3 = findViewById(R.id.radio3);
        radio4 = findViewById(R.id.radio4);
        radio5 = findViewById(R.id.radio5);
        radio6 = findViewById(R.id.radio6);

        radioGroup1 = findViewById(R.id.radioGroup1);
        radioGroup2 = findViewById(R.id.radioGroup2);

        // Change checked
        radio3.setChecked(true);
        radio5.setChecked(true);

        radioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                int checked = radioGroup.getCheckedRadioButtonId();
                switch (checked){
                    case R.id.radio1:
                        Toast.makeText(MainActivity.this, "R1 checked", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radio2:
                        Toast.makeText(MainActivity.this, "R2 checked", Toast.LENGTH_SHORT).show();
                        break;
                     case R.id.radio3:
                        Toast.makeText(MainActivity.this, "R3 checked", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });

        radioGroup2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                int checked = radioGroup.getCheckedRadioButtonId();
                switch (checked){
                    case R.id.radio4:
                        Toast.makeText(MainActivity.this, "R4 checked", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radio5:
                        Toast.makeText(MainActivity.this, "R5 checked", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radio6:
                        Toast.makeText(MainActivity.this, "R6 checked", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });



    }
}