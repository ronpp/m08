package com.example.kotlintest

class Person {
    var name: String
    var lastName: String
    var age: Int
    var balance :Double
    var isEmployee: Boolean


    constructor( name: String, lastName: String, age: Int, balance: Double, isEmployee:Boolean){
        this.name = name
        this.lastName = lastName
        this.age = age
        this.balance = balance
        this.isEmployee = isEmployee
    }
    init{
        age = 18
        balance = 100.0
        isEmployee = false
    }

}