package com.example.kotlintest

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {

    val CONSTANT = "com.example.kotlintest.CONSTANT"
    private lateinit var textResult : EditText
    private lateinit var button : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        textResult = findViewById(R.id.editText)
        button = findViewById(R.id.button)

        valVar()
        println("nullExample".uppercase())
        nullExample()

        button.setOnClickListener {
            var intent = Intent(this, NewActivity::class.java).apply{
                putExtra(CONSTANT, textResult.text)
            }
        }


    }


    private fun valVar(){
        var number1 = 100
        var number2 = 3_000_000_000
        var valid = false
        var number3 = 100.0
        var number4 = 100.0f
        var text1 = "Hello"
        var number5 = 100000L
        var number6 : Byte = 1
        var number7 : Unit = Unit

        // CONTANTES
        val TEXT2 = "Exit"
        val TEXT3 = "Exit2"
        val X: Int = 1
        val Y : Long = X.toLong()
        val Z : Double = Y.toDouble()
        // String.format("%d, %.2f", X, Z) // Java
        println(TEXT2 + "Hola $TEXT3")
        println(TEXT2 + "Hola ${TEXT3.uppercase()}")


    }

    private fun ifSentece(){
        var x= 1
    }

    private fun whenFunction(){
        var x = 10
        when (x){
            0 -> {
                println("$x is 0")}
            1,2,3 ->  { println("$x is 1, 2 o 3 ")}
            in 4..10 -> {println("$x is 4 ..10") }
            else -> {println("$x is else")  }
        }
    }

    private fun whenFunction2(){
        var country = "France"
        when (country) {
            "Spain" -> {println("$country is ES") }
            "France" -> {println("$country is FR") }
            "USA", "EEUU" -> {println("$country is US") }
            else -> {println("$country is unknown") }
        }
    }

    private fun arrayFunctions(){
        var person1 = Person("Ronny", "Peña", 30, 0.0, false )
        var person2 = Person("Ronny", "Peña", 30, 0.0, false )
        var person3 = Person("Ronny", "Peña", 30, 0.0, false )
        var person4 = Person("Ronny", "Peña", 30, 0.0, false )

        var arrayPerson = ArrayList<Person>()
        arrayPerson.add(person1)
        arrayPerson.add(person2)
        arrayPerson.add(person3)
        arrayPerson.add(person4)

        arrayPerson.forEach{
            println("${it.name} - ${it.lastName}")
        }

        for (element in arrayPerson){
            println("${element.name} - ${element.lastName}")
        }
    }

    private  fun nullExample(){
        var text : String? = "Value"
        text  = null
        println(text)
        println(text?.length)
    }
}