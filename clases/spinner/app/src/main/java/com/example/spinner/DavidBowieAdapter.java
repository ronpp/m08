package com.example.spinner;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class DavidBowieAdapter extends ArrayAdapter<DavidBowie> {

    // Constructor
    public DavidBowieAdapter(@NonNull Context context, ArrayList<DavidBowie> davidBowieArrayList) {
        super(context, 0, davidBowieArrayList);
    }


    // Control+o:
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return  init(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return  init(position, convertView, parent);
    }

    public View init(int position, @Nullable View convertView, @NonNull ViewGroup parent){
        //layout
        if (convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.spinner_row,
                    parent,
                    false
            );
        }
        // Hook
        ImageView imgCover = convertView.findViewById(R.id.imgCover);
        TextView txtCover = convertView.findViewById(R.id.txtCover);

        // setText and setImageResource
        DavidBowie currentItem = getItem(position);

        if (currentItem != null){
            imgCover.setImageResource(currentItem.getImgCover());
            txtCover.setText(currentItem.getCoverName());
        }
        return convertView;
    }
}
