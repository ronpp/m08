package com.example.spinner;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private Spinner spnString;
    private Spinner spnArray;
    private Spinner spnClass;
    private List<String> cursos2 = new ArrayList<>();

    private ArrayList<DavidBowie> mDavidBowie;
    private DavidBowieAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Hook
        spnString = findViewById(R.id.spnString);
        spnArray = findViewById(R.id.spnArray);
        spnClass = findViewById(R.id.spnClass);



        // SPINNER STRING

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_spinner_item,
                getResources().getStringArray(R.array.cursos)
        );
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_checked);

        spnString.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selected = parent.getItemAtPosition(position).toString();
                Toast.makeText(MainActivity.this, selected, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(MainActivity.this, "Nothing to show", Toast.LENGTH_SHORT).show();

            }
        });

        cursos2.add(0, "Select course");
        cursos2.add("ASI");
        cursos2.add("DAM");
        cursos2.add("DAW");


        // SPINNER ARRAY

        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_activated_1,
                cursos2
        );
        spnArray.setAdapter(adapter2);
        adapter2.setDropDownViewResource(android.R.layout.simple_list_item_activated_1);

        spnArray.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selection = parent.getItemAtPosition(position).toString();
                Toast.makeText(MainActivity.this, selection, Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(MainActivity.this, "Nothing to show", Toast.LENGTH_SHORT).show();

            }
        });

        // SPINNER CLASS
        initList();
        mAdapter = new DavidBowieAdapter(this, mDavidBowie);
        spnClass.setAdapter(mAdapter);
        spnClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                DavidBowie selectedItem = (DavidBowie)parent.getItemAtPosition(position);
                String selectedCover = selectedItem.getCoverName();
                Toast.makeText(MainActivity.this, selectedCover, Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(MainActivity.this, "Nothing Selected", Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void initList() {
        mDavidBowie = new ArrayList<>();
        mDavidBowie.add(new DavidBowie("Animals1", R.drawable.animals1));
        mDavidBowie.add(new DavidBowie("Animals2", R.drawable.animals2));
        mDavidBowie.add(new DavidBowie("Animals3", R.drawable.animals3));
        mDavidBowie.add(new DavidBowie("Animals4", R.drawable.animals4));
        mDavidBowie.add(new DavidBowie("Animals5", R.drawable.animals5));
    }
}