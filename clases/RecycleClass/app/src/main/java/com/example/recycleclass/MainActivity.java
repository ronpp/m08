package com.example.recycleclass;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recycleView;
    ArrayList<Animals> animals = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recycleView = findViewById(R.id.recycleView);

        initData();
        MyAdapter myAdapter = new MyAdapter(animals, this);
        recycleView.setAdapter(myAdapter);
        recycleView.setLayoutManager(new LinearLayoutManager(this));
        //recycleView.setLayoutManager(new GridLayoutManager(this, 2, RecyclerView.VERTICAL, false));
    }

    private void initData() {

        String[]meal1 ={"M01", "M02"};
        Animals animals1 = new Animals(
                "https://i.picsum.photos/id/1003/1181/1772.jpg?hmac=oN9fHMXiqe9Zq2RM6XT-RVZkojgPnECWwyEF1RvvTZk",
                "xx",
                "Animal 1",
                5,
                2015,
                meal1,
                "Descripcion animal 1");
                animals.add(animals1);

        String[]meal2 ={"M03", "M04", "M05"};
        Animals animals2 = new Animals(
                "https://i.picsum.photos/id/237/3500/2095.jpg?hmac=y2n_cflHFKpQwLOL1SSCtVDqL8NmOnBzEW7LYKZ-z_o",
                "xx",
                "Animal 2",
                5,
                2015,
                meal1,
                "Descripcion animal 2");
                animals.add(animals2);

        String[]meal3 ={"M06", "M07"};
        Animals animals3 = new Animals(
                "https://i.picsum.photos/id/1069/3500/2333.jpg?hmac=VBJ1vR2opkcKLS9NKGDl5uPxF02u6dSqbwc1x1b4oJc",
                "xx",
                "Animal3",
                7,
                2017,
                meal3,
                "Descripcion animal 3");
                animals.add(animals3);

        String[]meal4 ={"M08", "M09", "M10"};
        Animals animals4 = new Animals(
                "https://i.picsum.photos/id/1074/5472/3648.jpg?hmac=w-Fbv9bl0KpEUgZugbsiGk3Y2-LGAuiLZOYsRk0zo4A",
                "xx",
                "Animal3",
                8,
                2018,
                meal4,
                "Descripcion animal 4");
                animals.add(animals4);

        String[]meal5 ={"M11", "M12"};
        Animals animals5 = new Animals(
                "https://i.picsum.photos/id/219/5184/3456.jpg?hmac=2LU7i3c6fykd_J0T6rZm1aBoBmK4ivkH1Oc459aRUU0",
                "xx",
                "Animal5",
                5,
                2015,
                meal5,
                "Descripcion animal 5");
                animals.add(animals5);

        String[]meal6 ={"M13", "M14", "M15"};
        Animals animals6 = new Animals(
                "https://i.picsum.photos/id/1024/1920/1280.jpg?hmac=-PIpG7j_fRwN8Qtfnsc3M8-kC3yb0XYOBfVzlPSuVII",
                "xx",
                "Animal6",
                6,
                2016,
                meal6,
                "Descripcion animal 6");
                animals.add(animals6);
    }
}