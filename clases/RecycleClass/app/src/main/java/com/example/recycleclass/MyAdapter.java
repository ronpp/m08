package com.example.recycleclass;



import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private ArrayList<Animals>animals;
    Context context;

    public MyAdapter(ArrayList<Animals> animals, Context context) {
        this.animals = animals;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.row_data, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Picasso.get().load(animals.get(position).getUrlImage())
                .fit()
                .centerCrop()
                .into(holder.imgAnimal);
        holder.txtType.setText(animals.get(position).getType());
        holder.txtNickname.setText(animals.get(position).getNickName());
        holder.txtAge.setText(String.valueOf(animals.get(position).getAge()));
        holder.txtBirthdate.setText(String.valueOf(animals.get(position).getBirthdateYear()));
        holder.txtMeals.setText(animals.get(position).getMealsString());
        //part2
        holder.rowLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int myPosition = holder.getAdapterPosition();
                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra("type", animals.get(myPosition).getType());
                intent.putExtra("nickname", animals.get(myPosition).getNickName());
                intent.putExtra("age", animals.get(myPosition).getAge());
                intent.putExtra("birthyear", animals.get(myPosition).getBirthdateYear());
                intent.putExtra("meals", animals.get(myPosition).getMealsString());
                intent.putExtra("desc",animals.get(myPosition).getDescription());
                intent.putExtra("urlImage",animals.get(myPosition).getUrlImage());
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return animals.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        private ImageView imgAnimal;
        private TextView txtType;
        private TextView txtNickname;
        private TextView txtAge;
        private TextView txtBirthdate;
        private TextView txtMeals;
        ConstraintLayout rowLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imgAnimal = itemView.findViewById(R.id.imgAnimal);
            txtType = itemView.findViewById(R.id.txtTypeValue);
            txtNickname = itemView.findViewById(R.id.txtNicknameValue);
            txtAge = itemView.findViewById(R.id.txtAgeValue);
            txtBirthdate = itemView.findViewById(R.id.txtBirthdateValue);
            txtMeals = itemView.findViewById(R.id.txtMealValue);
            rowLayout = itemView.findViewById(R.id.rowLayout);


        }
    }
}