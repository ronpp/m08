package com.example.recycleclass;

public class Animals {

    private String urlImage;
    private String type;
    private String nickName;
    private int age;
    private int birthdateYear;
    private String[] meals;
    private String description;

    public Animals(String urlImage, String type, String nickName, int age, int birthdateYear, String[] meals, String description) {
        this.urlImage = urlImage;
        this.type = type;
        this.nickName = nickName;
        this.age = age;
        this.birthdateYear = birthdateYear;
        this.meals = meals;
        this.description = description;
    }


    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getBirthdateYear() {
        return birthdateYear;
    }

    public void setBirthdateYear(int birthdateYear) {
        this.birthdateYear = birthdateYear;
    }

    public String[] getMeals() {
        return meals;
    }

    public void setMeals(String[] meals) {
        this.meals = meals;
    }

    public String getMealsString(){
        String value = "";
        for (int i = 0; i < meals.length; i++) {
            if (i == 0) {
                value = meals[i];
            }else {
                value = value + ", " + meals[i];
            }
        }
        return value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
