package com.example.recycleclass;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity {
    private TextView txtType;
    private TextView txtNickname;
    private TextView txtAge;
    private TextView txtBirthYear;
    private TextView txtMeals;
    private TextView txtDesc;
    private ImageView imgDetail;
    String type, nickname, meals, desc, urlImage;
    int age, birthYear;

    private void getData(){
        if(getIntent().hasExtra("type") && getIntent().hasExtra("nickname")){
            type = getIntent().getStringExtra("type");
            nickname = getIntent().getStringExtra("nickname");
            age = getIntent().getIntExtra("age", 1);
            birthYear = getIntent().getIntExtra("birthyear", 1);
            meals = getIntent().getStringExtra("meals");
            desc = getIntent().getStringExtra("desc");
            urlImage = getIntent().getStringExtra("urlImage");
        } else{
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
        }
    }

    private void setData(){
        txtType.setText(type);
        txtNickname.setText(nickname);
        txtAge.setText(String.valueOf(age));
        txtBirthYear.setText(String.valueOf(birthYear));
        txtMeals.setText(meals);
        txtDesc.setText(desc);
        Picasso.get().load(urlImage)
                .fit()
                .centerCrop()
                .into(imgDetail);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        //Hook
        txtType = findViewById(R.id.txtTypeDetailsValue);
        txtNickname = findViewById(R.id.txtNicknameDetailsValue);
        txtAge = findViewById(R.id.txtAgeDetailsValue);
        txtBirthYear = findViewById(R.id.txtBirthdateDetailsValue);
        txtMeals = findViewById(R.id.txtMealDetailsValue);
        txtDesc = findViewById(R.id.txtDescDetailsValue);
        imgDetail = findViewById(R.id.imgAnimalDetails);

        getData();
        setData();

    }
}