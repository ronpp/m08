package com.example.edt03_sharedpreferences;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

public class MainActivity extends AppCompatActivity {

    public static final String PREF_FILE_NAME = "MySharedFile";

    private TextInputEditText txtName;
    private TextInputEditText txtEmail;
    private TextInputEditText txtPhone;

    private Button btnSave;
    private Button btnLoad;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Hook
        txtName = findViewById(R.id.txtName);
        txtEmail = findViewById(R.id.txtEmail);
        txtPhone = findViewById(R.id.txtPhone);
        btnSave = findViewById(R.id.btnSave);
        btnLoad = findViewById(R.id.btnLoad);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences = getSharedPreferences(PREF_FILE_NAME,
                        Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("name", txtName.getText().toString());
                editor.putString("email", txtEmail.getText().toString());
                editor.putString("phone", txtPhone.getText().toString());
                editor.apply();
                Toast.makeText(MainActivity.this, "Data Saved", Toast.LENGTH_SHORT).show();
            }
        });

        btnLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences = getSharedPreferences(PREF_FILE_NAME,
                        Context.MODE_PRIVATE);
                String name = sharedPreferences.getString("name", "No data found");
                String email = sharedPreferences.getString("email", "No data found");
                String phone = sharedPreferences.getString("phone", "No data found");
                txtName.setText(name);
                txtEmail.setText(email);
                txtPhone.setText(phone);
                Toast.makeText(MainActivity.this, "Data Load", Toast.LENGTH_SHORT).show();
            }
        });
    }
}