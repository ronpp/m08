package com.example.transicion;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private TextView txtTitle;
    private TextView txtLoginTitle;
    private ImageView imgLogo;
    private ImageView imgLoginLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Hook
        txtTitle = findViewById(R.id.txtTitle);
        txtLoginTitle = findViewById(R.id.txtLoginTitle);
        imgLogo = findViewById(R.id.imgLogo);
        imgLoginLogo = findViewById(R.id.imgLoginLogo);

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(MainActivity.this,activity_login.class);
                Pair[] pairs = new Pair[2];
                pairs[0] = new Pair<View,String>(imgLogo,"image_origen");
                pairs[1] = new Pair<View,String>(txtTitle,"textapp_origen");
                //Works only fro API > 21, then we need to surronded by  this
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this, pairs);
                    startActivity(intent,options.toBundle());
                }

            }
        }, 4000);
    }
}