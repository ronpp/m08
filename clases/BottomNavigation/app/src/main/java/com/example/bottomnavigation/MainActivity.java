package com.example.bottomnavigation;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView mMainNav;
    private FrameLayout mMainFrame;
    private HomeFragment homeFragment;
    private ProfileFragment profileFragment;
    private SettingsFragment settingsFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Hook
        mMainNav = (BottomNavigationView) findViewById(R.id.main_nav);
        mMainFrame = (FrameLayout) findViewById(R.id.main_frame);

        homeFragment = HomeFragment.newInstance("Home1", "Home2");
        profileFragment = ProfileFragment.newInstance("Profile1", "Profile2");
        settingsFragment = SettingsFragment.newInstance("Settings1", "Settings2");

        setFragment(homeFragment);

        // Listener
        mMainNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.nav_home:
                        //open the HomeFragment
                        setFragment(homeFragment);
                        //We can change the color of the background using the following:
                        mMainNav.setItemBackgroundResource(R.color.primaryDarkColor);
                        return true;
                    case R.id.nav_profile:
                        //Change color for this exercise
                        mMainNav.setItemBackgroundResource(R.color.primaryColor);
                        //open the ProfileFragment
                        setFragment(profileFragment);
                        return true;
                    case R.id.nav_settings:
                        //Change color for this exercise
                        mMainNav.setItemBackgroundResource(R.color.secondaryLightColor);
                        //open the SettingsFragment
                        setFragment(settingsFragment);
                        return true;
                    default:
                        return false;
                }
            }
        });


    }

    private void setFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_frame, fragment);
        fragmentTransaction.commit();

    }
}