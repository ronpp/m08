package com.example.edt38;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private List<Peak> peaks = new ArrayList<>();
    private static String JSON_URL = "https://run.mocky.io/v3/215b6d3c-bae5-4f42-9230-cd15b0780baf";
    MyAdapter myAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Hook

        recyclerView = findViewById(R.id.recyclePeak);

        getPeaks();
//        recyclerView.setLayoutManager(new LinearLayoutManager(this));
//        MyAdapter myAdapter = new MyAdapter(this, peaks);
//        recyclerView.setAdapter(myAdapter);
    }


    private void getPeaks(){
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                JSON_URL,
                null,
                new  Response.Listener<JSONArray>(){
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject peakObj = response.getJSONObject(i);
                                Peak peak = new Peak();
                                peak.setName(peakObj.getString("name"));
                                peak.setHeight(peakObj.getString("height"));
                                peak.setProminence(peakObj.getString("prominence"));
                                peak.setZone(peakObj.getString("zone"));
                                peak.setUrl(peakObj.getString("url"));
                                peak.setCountry(peakObj.getString("country"));
                                peaks.add(peak);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        myAdapter = new MyAdapter(getApplicationContext(), peaks);
                        recyclerView.setAdapter(myAdapter);
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("tag", "onErrorResponse: " + error.getMessage());
                    }
                }
          );


        requestQueue.add(jsonArrayRequest);
    }
}