package com.example.edt38;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity {

    private TextView txtNameDtail;
    private TextView txtHeigthDtail;
    private TextView txtProminenceDtail;
    private TextView txtZoneDtail;
    private TextView txtCountryDtail;
    private ImageView imgDetail;

    private  String name, height, prominence, zone, url, country;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        // Hook

        txtNameDtail = findViewById(R.id.txtNameDtail);
        txtHeigthDtail = findViewById(R.id.txtHeightDtail);
        txtProminenceDtail = findViewById(R.id.txtProminenceDtail);
        txtZoneDtail = findViewById(R.id.txtZoneDtail);
        txtCountryDtail = findViewById(R.id.txtCountryDtail);
        imgDetail = findViewById(R.id.imgDetails);

        getData();
        setData();

    }

    private void getData(){
        Intent i =getIntent();
        if (i.hasExtra("name") && i.hasExtra("height")) {
            name = i.getStringExtra("name");
            height = i.getStringExtra("height");
            prominence = i.getStringExtra("prominence");
            zone = i.getStringExtra("zone");
            country = i.getStringExtra("country");
            url = i.getStringExtra("url");
        } else {
            Toast.makeText(this, "No Data Found", Toast.LENGTH_SHORT).show();
        }
    }

   private void setData(){
        txtNameDtail.setText(name);
        txtHeigthDtail.setText(height);
        txtProminenceDtail.setText(prominence);
        txtZoneDtail.setText(zone);
        txtCountryDtail.setText(country);
       Picasso.get().load(url)
               .fit()
               .centerCrop()
               .into(imgDetail);
   }
}