package com.example.edt24_viewpageimage;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private TextView txtRight;
    private TextView txtLeft;
    private String[] imageUrls = new String[]{
            "https://www.joanseculi.com/images/image01.jpg",
            "https://www.joanseculi.com/images/image02.jpg",
            "https://www.joanseculi.com/images/image03.jpg",
            "https://www.joanseculi.com/images/image04.jpg",
            "https://www.joanseculi.com/images/image05.jpg",

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // hook
        viewPager = findViewById(R.id.viewPager);
        txtLeft = findViewById(R.id.txtLeft);
        txtRight = findViewById(R.id.txtRigth);

        ImageAdapter imageAdapter = new ImageAdapter(this, imageUrls);
        viewPager.setAdapter(imageAdapter);

        txtRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.arrowScroll(View.FOCUS_RIGHT);
            }
        });

        txtLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.arrowScroll(View.FOCUS_LEFT);
            }
        });
    }
}