package com.example.marvel.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marvel.DetailActivity;
import com.example.marvel.R;
import com.example.marvel.models.Movie;
import com.squareup.picasso.Picasso;

public class MovieAdapter  extends RecyclerView.Adapter<MovieAdapter.MyViewHolder>{

    private Context context;
    private Movie[] movies;

    public MovieAdapter(Context context, Movie[] movies) {
        this.context = context;
        this.movies = movies;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.movie_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.movieTitle.setText(movies[position].getTitle());
        holder.movieDescription.setText(movies[position].getDescription());
        holder.moviePubliDate.setText(movies[position].getPublishedAt());
        Picasso.get().load(movies[position].getUrlToImage())
                .fit()
                .centerCrop()
                .into(holder.imgMovie);

        holder.movieLayout.setOnClickListener(view -> {
            Intent intent = new Intent(context, DetailActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("movie", movies[position]);
            intent.putExtras(bundle);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return movies.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView movieTitle;
        TextView movieDescription;
        TextView moviePubliDate;
        ImageView imgMovie;
        ConstraintLayout movieLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            movieTitle = itemView.findViewById(R.id.txtMovieTitle);
            movieDescription = itemView.findViewById(R.id.txtMovieDescription);
            moviePubliDate = itemView.findViewById(R.id.txtMoviePubliDate);
            imgMovie = itemView.findViewById(R.id.imgMovie);
            movieLayout = itemView.findViewById(R.id.movieLayout);
        }
    }
}
