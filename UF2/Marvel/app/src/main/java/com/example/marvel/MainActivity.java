package com.example.marvel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.marvel.adapters.MovieAdapter;
import com.example.marvel.models.Movie;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;

public class MainActivity extends AppCompatActivity {

    private static final String URL = "https://run.mocky.io/v3/39fcc41e-9d03-486c-8fb2-235e3e831a1b";

    private RecyclerView recyclerMovie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Hook
        recyclerMovie = findViewById(R.id.recyclerMovie);

        // Request
        RequestQueue requestQueue =  Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                response -> {
                    String status = response.optString("status");
                    if("ok".equals(status)){
                        JSONArray jsonArray = response.optJSONArray("articles");
                        Movie[] movies = new GsonBuilder().create().fromJson(jsonArray.toString(),
                                Movie[].class);
                        MovieAdapter movieAdapter = new MovieAdapter(getApplicationContext(), movies);
                        recyclerMovie.setAdapter(movieAdapter);
                        recyclerMovie.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    }
                },
                error -> {
                    Toast.makeText(getApplicationContext(),"Something Wrong", Toast.LENGTH_SHORT).show();
                }
        );
        requestQueue.add(request);
    }
}