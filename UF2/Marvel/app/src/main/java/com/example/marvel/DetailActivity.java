package com.example.marvel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.marvel.adapters.CommentAdapter;
import com.example.marvel.adapters.ImageAdapter;
import com.example.marvel.models.Movie;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;
import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity {

    private ImageView imgMovieDetail;
    private TextView txtMovieTitleDtail;
    private TextView txtMovieDateDtail;
    private TextView txtMovieDescDtail;
    private TextView txtMovieContentDtail;
    private TextView txtMovieAuthorNameDtail;
    private Button btnWeb;
    private Button btnAuthor;
    private Button btnContact;
    private ViewPager slideMovieImages;
    private RecyclerView recyclerComments;
    private YouTubePlayerView youTubePlayerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        // Hooks
        imgMovieDetail = findViewById(R.id.imgMovieDetail);
        txtMovieTitleDtail = findViewById(R.id.txtMovieTitleDtail);
        txtMovieDateDtail = findViewById(R.id.txtMovieDateDtail);
        txtMovieDescDtail = findViewById(R.id.txtMovieDescDtail);
        txtMovieContentDtail = findViewById(R.id.txtMovieContentDtail);
        txtMovieAuthorNameDtail = findViewById(R.id.txtMovieAuthorNameDtail);
        btnWeb = findViewById(R.id.btnWeb);
        btnAuthor = findViewById(R.id.btnAuthor);
        btnContact = findViewById(R.id.btnContact);
        slideMovieImages = findViewById(R.id.slideMovieImages);
        recyclerComments = findViewById(R.id.recyclerComments);
        youTubePlayerView = findViewById(R.id.youtube_player_view);

        // Get data
        Bundle bundle = getIntent().getExtras();
        Movie movie = (Movie) bundle.getSerializable("movie");

        // Set Data
        Picasso.get().load(movie.getUrlToImage())
                .fit()
                .centerCrop()
                .into(imgMovieDetail);

        txtMovieTitleDtail.setText(movie.getTitle());
        txtMovieDateDtail.setText(movie.getPublishedAt());
        txtMovieDescDtail.setText(movie.getDescription());
        txtMovieContentDtail.setText(movie.getContent());
        txtMovieAuthorNameDtail.setText(movie.getAuthorname());

        // Slide Images
        String[] images = {
                movie.getMedia().getUrlToImage1(),
                movie.getMedia().getUrlToImage2(),
                movie.getMedia().getUrlToImage3(),
        };
        ImageAdapter imageAdapter = new ImageAdapter(getApplicationContext(), images);
        slideMovieImages.setAdapter(imageAdapter);

        // Video
        getLifecycle().addObserver(youTubePlayerView);
        youTubePlayerView.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
            @Override
            public void onReady(YouTubePlayer youTubePlayer) {
                String[] urlParts =  movie.getUrlvideo().split("/");
                String videoId = urlParts[urlParts.length - 1];
                youTubePlayer.cueVideo(videoId, 0);
            }
        });


        // Comments
        CommentAdapter commentAdapter = new CommentAdapter(getApplicationContext(), movie.getComments());
        recyclerComments.setAdapter(commentAdapter);
        recyclerComments.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        // Buttons
        btnWeb.setOnClickListener(view -> {
            String url = movie.getUrl();;
            // we Added "https://" to url, otherwise the app crashed
            url = url.startsWith("https://")? url : "https://"+ url;
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            getApplicationContext().startActivity(i);
        });

        btnAuthor.setOnClickListener(view -> {
            String url = movie.getAuthorlink();;
            // we Added "https://" to url, otherwise the app crashed
            url = url.startsWith("https://")? url : "https://"+ url;
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            getApplicationContext().startActivity(i);
        });

        btnContact.setOnClickListener(view -> {
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setData(Uri.parse("mailto:"));
            emailIntent.setType("text/plain");
            String[] TO = {movie.getContact()};
            emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject of the mail");
            emailIntent.putExtra(Intent.EXTRA_TEXT, "This is the text to send wiht mail");
            try {
                startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            }catch (android.content.ActivityNotFoundException ex){
                Toast.makeText(getApplicationContext(), "There not email client installed",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
}