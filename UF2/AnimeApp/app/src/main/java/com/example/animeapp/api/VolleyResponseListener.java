package com.example.animeapp.api;

public interface VolleyResponseListener <T>{
    void onResponse(T response);

}
