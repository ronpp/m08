package com.example.animeapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.animeapp.R;
import com.example.animeapp.fragments.AnimeDetailFragment;
import com.example.animeapp.model.Anime;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AnimeAdapter extends RecyclerView.Adapter<AnimeAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<Anime> animes;

    public AnimeAdapter(Context context, ArrayList<Anime> animes) {
        this.context = context;
        this.animes = animes;
    }

    @NonNull
    @Override
    public AnimeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.anime_layout, parent,  false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AnimeAdapter.MyViewHolder holder, int position) {
        holder.animeName.setText(animes.get(position).getName());
        holder.animeScore.setText(String.valueOf(animes.get(position).getScore()));
        holder.animeEpisodes.setText(String.valueOf(animes.get(position).getEpisodes()));
        Picasso.get().load(animes.get(position).getImageUrl())
                .fit()
                .centerCrop()
                .into(holder.animeImg);

        holder.animeLayout.setOnClickListener(view -> {
                String animeid = animes.get(position).getAnimeid();
                String animeName = animes.get(position).getName();
                String animeType = animes.get(position).getType();
                String episodes = String.valueOf(animes.get(position).getEpisodes());
                String year = String.valueOf(animes.get(position).getYear_release());
                String desc = animes.get(position).getDescription();
                String imageUrl = animes.get(position).getImageUrl();
                AnimeDetailFragment animeDetailFragment = AnimeDetailFragment.newInstance(animeid,
                        animeName, animeType, episodes, year, desc, imageUrl);
            FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.main_frame, animeDetailFragment);
            fragmentTransaction.commit();

        });
    }

    @Override
    public int getItemCount() {
        return animes.size();
    }

    public class MyViewHolder extends  RecyclerView.ViewHolder{
        ImageView animeImg;
        TextView animeName;
        TextView animeScore;
        TextView animeEpisodes;
        ConstraintLayout animeLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            animeName = itemView.findViewById(R.id.animeName);
            animeScore = itemView.findViewById(R.id.animeScore);
            animeEpisodes = itemView.findViewById(R.id.animeEpisodes);
            animeImg = itemView.findViewById(R.id.animeImg);
            animeLayout = itemView.findViewById(R.id.animeLayout);
        }
    }

}
