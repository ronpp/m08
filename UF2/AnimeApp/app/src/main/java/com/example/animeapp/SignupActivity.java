package com.example.animeapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class SignupActivity extends AppCompatActivity {

    private TextView txtSignIn;
    private TextView txtTServices;
    private TextView txtPrivacy;
    private Button btnSignup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);


        // Hook
        txtSignIn = findViewById(R.id.txtSignIn);
        txtTServices = findViewById(R.id.txtTServices);
        txtPrivacy = findViewById(R.id.txtPrivacy);
        btnSignup = findViewById(R.id.btnSignup);

        
         // Listeners

        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignupActivity.this, MainActivity.class));
            }
        });


        txtSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignupActivity.this, LoginActivity.class));
            }
        });
        
        txtTServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tServices = "These are the terms of services of AnimeTxT Inc.";
                Toast.makeText(SignupActivity.this, tServices, Toast.LENGTH_SHORT).show();
            }
        });

        txtPrivacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String privacy = "These are the privacy policies of AnimeTxT Inc.";
                Toast.makeText(SignupActivity.this, privacy, Toast.LENGTH_SHORT).show();
            }
        });
    }
}