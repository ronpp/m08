package com.example.animeapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.animeapp.R;
import com.example.animeapp.api.AnimeService;
import com.example.animeapp.fragments.FavoriteFragment;
import com.example.animeapp.model.Favorite;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<Favorite> favorites;

    public FavoriteAdapter(Context context, ArrayList<Favorite> favorites) {
        this.context = context;
        this.favorites = favorites;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.favorite_layout, parent,  false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.favoriteName.setText(favorites.get(position).getName());
        Picasso.get()
                .load(favorites.get(position).getImageurl())
                .fit()
                .centerCrop()
                .into(holder.favoriteImg);
        holder.favoriteIcon.setChecked(true);
        holder.favoriteIcon.setOnClickListener(v ->{
            AnimeService animeService = new AnimeService(context);
            animeService.deleteFavorite(favorites.get(position).getAnimeid(), response -> {
                if(response){
                    FavoriteFragment favoriteFragment = new FavoriteFragment();
                    FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.main_frame, favoriteFragment);
                    fragmentTransaction.commit();
                }
            });
        });
    }

    @Override
    public int getItemCount() {
        return favorites.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView favoriteImg;
        TextView favoriteName;
        MaterialCheckBox favoriteIcon;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            favoriteImg = itemView.findViewById(R.id.favoriteImg);
            favoriteName = itemView.findViewById(R.id.favoriteName);
            favoriteIcon = itemView.findViewById(R.id.favoriteIcon);
        }
    }
}
