package com.example.animeapp;

import androidx.appcompat.app.AppCompatActivity;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.animeapp.api.AnimeService;
import com.google.android.material.textfield.TextInputEditText;

public class LoginActivity extends AppCompatActivity {

    private TextView txtSignup;
    private Button btnLogin;
    private TextInputEditText txtLoginUser;
    private TextInputEditText txtLoginPass;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Hook
        txtSignup = findViewById(R.id.txtSignUp);
        btnLogin = findViewById(R.id.btnLogin);
        txtLoginUser = findViewById(R.id.txtLoginUser);
        txtLoginPass = findViewById(R.id.txtLoginPass);


        // Listeners

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = txtLoginUser.getText().toString();
                String pass = txtLoginPass.getText().toString();
                AnimeService animeService = new AnimeService(getApplicationContext());
                animeService.login(username, pass,
                        response -> {
                            if (response) {
                                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            }
                        });
            }
        });

        txtSignup.setOnClickListener(view -> startActivity(new Intent(LoginActivity.this, SignupActivity.class)));
    }
}