package com.example.animeapp.model;

import android.util.Base64;

public class User {

    private static User instance;
    private String username;
    private String password;
    private static String auth;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
        String credentials = String.format("%s:%s",username,password);
        auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
    }

    public String getUsername() {
        return username;
    }

    public static String getAuth() {
        return auth;
    }

}
