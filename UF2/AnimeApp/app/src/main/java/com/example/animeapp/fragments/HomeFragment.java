package com.example.animeapp.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.animeapp.R;
import com.example.animeapp.adapters.AnimeAdapter;
import com.example.animeapp.api.AnimeService;
import com.example.animeapp.model.Anime;
import com.example.animeapp.model.Favorite;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private RecyclerView animeRecycler;
    private RecyclerView animeMovieRecycler;
    private String mParam1;
    private String mParam2;


    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_home, container, false);

        // TV Series
        AnimeService animeService = new AnimeService(getContext());
        animeService.getAnimes(response -> {
            ArrayList<Anime> tv = new ArrayList<>();
            for (Anime anime : response) {
                if (anime.getType().equals("TV")) {
                    tv.add(anime);
                }
            }
            AnimeAdapter animeAdapter = new AnimeAdapter(getContext(), tv);
            animeRecycler = view.findViewById(R.id.animeRecycler);
            animeRecycler.setAdapter(animeAdapter);
            animeRecycler.setLayoutManager(
                    new LinearLayoutManager(getContext(),
                            LinearLayoutManager.HORIZONTAL,
                            false));

        });

        // Film
        animeService.getAnimes(response -> {
            ArrayList<Anime> film = new ArrayList<>();
            for (Anime anime : response) {
                if (anime.getType().equals("Film")) {
                    film.add(anime);
                }
            }
            AnimeAdapter animeAdapter = new AnimeAdapter(getContext(), film);
            animeMovieRecycler = view.findViewById(R.id.animeMovieRecycler);
            animeMovieRecycler.setAdapter(animeAdapter);
            animeMovieRecycler.setLayoutManager(
                    new LinearLayoutManager(getContext(),
                            LinearLayoutManager.HORIZONTAL,
                            false));

        });
        return view;
    }
}