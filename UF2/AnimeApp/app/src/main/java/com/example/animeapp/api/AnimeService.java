package com.example.animeapp.api;

import android.content.Context;
import android.util.Base64;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.animeapp.model.Anime;
import com.example.animeapp.model.Favorite;
import com.example.animeapp.model.User;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AnimeService {

    public static final String BASE_URL = "https://animeapim06.herokuapp.com";
    Context context;

    public AnimeService(Context context) {
        this.context = context;
    }

    public  void login(String username, String password, VolleyResponseListener<Boolean> vrl){
        String url = BASE_URL + "/users/login/";
        StringRequest request = new StringRequest(Request.Method.GET,  url,
                response -> vrl.onResponse(true),
                error -> Toast.makeText(this.context, "ACCESS DENIED", Toast.LENGTH_LONG ).show()
                ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                String credentials = String.format("%s:%s",username,password);
                User user = new User(username, password);
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }
        };
       RequestSingleton.getInstance(context).addToRequestQueue(request);
    }


    public void getAnimes(VolleyResponseListener<ArrayList<Anime>> vrl){
        String url = BASE_URL + "/animes/";
        ArrayList<Anime> animeList = new ArrayList<>();
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                url,
                null,
                response -> {
                    try {
                        JSONArray animeJsonList =   response.getJSONArray("result");
                        for (int i = 0; i < animeJsonList.length(); i++) {
                            JSONObject animeObject = (JSONObject) animeJsonList.get(i);
                            Anime anime = new Anime();
                            anime.setAnimeid(animeObject.getString("animeid"));
                            anime.setName(animeObject.getString("name"));
                            anime.setType(animeObject.getString("type"));
                            anime.setYear_release(animeObject.getInt("year_release"));
                            anime.setEpisodes(animeObject.getInt("episodes"));
                            anime.setImageUrl(animeObject.getString("imageurl"));
                            anime.setDescription(animeObject.getString("description"));
                            anime.setScore(0.0);
                            animeList.add(anime);
                        }
                        vrl.onResponse(animeList);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                },
                error -> Toast.makeText(context, "AN ERROR OCCURRED", Toast.LENGTH_SHORT).show()
        );
        RequestSingleton.getInstance(context).addToRequestQueue(request);
    }

    public void getFavorite(VolleyResponseListener<ArrayList<Favorite>> vrl){
        String url = BASE_URL + "/users/favorites/";
        ArrayList<Favorite> favorites = new ArrayList<>();
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                url,
                null,
                response -> {
                    try {
                        JSONArray animeJsonList =   response.getJSONArray("result");
                        for (int i = 0; i < animeJsonList.length(); i++) {
                            JSONObject favObject = (JSONObject) animeJsonList.get(i);
                            String animeid = favObject.getString("animeid");
                            String name = favObject.getString("name");
                            String imageurl = favObject.getString("imageurl");
                            Favorite favorite = new Favorite(animeid, name, imageurl);
                            favorites.add(favorite);
                        }
                        vrl.onResponse(favorites);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> Toast.makeText(context, "AN ERROR OCCURRED", Toast.LENGTH_SHORT).show()
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                String auth = User.getAuth();
                params.put("Authorization", auth);
                return params;
            }
        };
        RequestSingleton.getInstance(context).addToRequestQueue(request);
    }


    public void addFavorite(String animeId ,VolleyResponseListener<Boolean> vrl){
        String url = BASE_URL + "/users/favorites/";
        StringRequest request = new StringRequest(Request.Method.POST,  url,
                response -> vrl.onResponse(true),
                error -> Toast.makeText(this.context, "AN ERROR OCCURRED", Toast.LENGTH_LONG ).show()
        ){

            @Override
            public byte[] getBody() throws AuthFailureError {
                JSONObject jsonBody = new JSONObject();
                try {
                    jsonBody.put("animeid", animeId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return jsonBody.toString().getBytes(StandardCharsets.UTF_8);
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                String auth = User.getAuth();
                params.put("Authorization", auth);
                params.put("Content-Type", "application/json; charset=utf-8");
                return params;
            }
        };
        RequestSingleton.getInstance(context).addToRequestQueue(request);
    }


    public void deleteFavorite(String animeId ,VolleyResponseListener<Boolean> vrl){
        String url = BASE_URL + "/users/favorites/" +animeId;
        StringRequest request = new StringRequest(Request.Method.DELETE,  url,
                response -> vrl.onResponse(true),
                error -> Toast.makeText(this.context, "AN ERROR OCCURRED", Toast.LENGTH_LONG ).show()
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                String auth = User.getAuth();
                params.put("Authorization", auth);
                return params;
            }
        };
        RequestSingleton.getInstance(context).addToRequestQueue(request);
    }


}
