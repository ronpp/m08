package com.example.animeapp.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.animeapp.R;
import com.example.animeapp.api.AnimeService;
import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AnimeDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AnimeDetailFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    private static final String ANIME_ID = "param1";
    private static final String ANIME_NAME = "param2";
    private static final String ANIME_TYPE = "param3";
    private static final String ANIME_EPISODES = "param4";
    private static final String ANIME_YEAR = "param5";
    private static final String ANIME_DESCRIPTION = "param6";
    private static final String ANIME_IMAGE_URL = "param7";

    // TODO: Rename and change types of parameters

    private String animeId;
    private String animeName;
    private String animeType;
    private String episodes;
    private String year;
    private String desc;
    private String imageUrl;

    public AnimeDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param animeName Parameter 1.
     * @param animeType Parameter 2.
     * @param episodes Parameter 3.
     * @param year Parameter 4.
     * @param desc Parameter 5.
     * @param imageUrl Parameter 6.
     * @return A new instance of fragment AnimeDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AnimeDetailFragment newInstance(String animeId, String animeName, String animeType, String episodes, String year, String desc, String imageUrl) {
        AnimeDetailFragment fragment = new AnimeDetailFragment();
        Bundle args = new Bundle();
        args.putString(ANIME_ID, animeId);
        args.putString(ANIME_NAME, animeName);
        args.putString(ANIME_TYPE, animeType);
        args.putString(ANIME_EPISODES, episodes);
        args.putString(ANIME_YEAR, year);
        args.putString(ANIME_DESCRIPTION, desc);
        args.putString(ANIME_IMAGE_URL, imageUrl);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            animeId = getArguments().getString(ANIME_ID);
            animeName = getArguments().getString(ANIME_NAME);
            animeType = getArguments().getString(ANIME_TYPE);
            episodes = getArguments().getString(ANIME_EPISODES);
            year = getArguments().getString(ANIME_YEAR);
            desc = getArguments().getString(ANIME_DESCRIPTION);
            imageUrl = getArguments().getString(ANIME_IMAGE_URL);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_anime_detail, container, false);

        // Hook
        TextView txtDetailName = view.findViewById(R.id.txtDetailName);
        TextView txtDetailType = view.findViewById(R.id.txtDetailType);
        TextView txtDetailEpi = view.findViewById(R.id.txtDetailEpi);
        TextView txtDetailYear = view.findViewById(R.id.txtDetailYear);
        TextView txtDetailDesc = view.findViewById(R.id.txtDetailDesc);
        ImageView detailImg = view.findViewById(R.id.detailImg);
        Button btnFavorite = view.findViewById(R.id.btnFav);

        btnFavorite.setOnClickListener(v ->{
            AnimeService animeService = new AnimeService(getContext());
            animeService.addFavorite(animeId, response -> {
                if(response){
                    String msg = String.format("Added the anime '%s' to favorites", animeName);
                    Toast.makeText(getContext(),msg, Toast.LENGTH_SHORT).show();
                }
            });
        });

        // Set data
        txtDetailName.setText(animeName);
        txtDetailType.setText(animeType);
        txtDetailEpi.setText(episodes);
        txtDetailYear.setText(year);
        txtDetailDesc.setText(desc);
        Picasso.get().load(imageUrl)
                .fit()
                .centerCrop()
                .into(detailImg);

        return view;
    }
}