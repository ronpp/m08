package com.example.animeapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.animeapp.fragments.FavoriteFragment;
import com.example.animeapp.fragments.HomeFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private RecyclerView animeRecycler;
    private RecyclerView animeTopRecycler;
    private BottomNavigationView main_nav;

    private HomeFragment homeFragment;
    private FavoriteFragment favoriteFragment;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hook
        toolbar = findViewById(R.id.toolbar);
        animeRecycler = findViewById(R.id.animeRecycler);
        animeTopRecycler = findViewById(R.id.animeMovieRecycler);
        main_nav = findViewById(R.id.main_nav);

        //  Set toolbar
        setSupportActionBar(toolbar);


        // Init Fragment
        homeFragment = new HomeFragment();
        favoriteFragment = new FavoriteFragment();

        // Default Fragment
        setFragment(homeFragment);

        // Set Nav menu listener
        main_nav.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()){
                case R.id.home_nav:
                    setFragment(homeFragment);
                    return true;
                case R.id.favorite_nav:
                    setFragment(favoriteFragment);
                    return true;
                case R.id.recommended_nav:
                    //setFragment(recomendedFragment);
                    Toast.makeText(this, "PENDING TO IMPLEMENT", Toast.LENGTH_SHORT).show();
                    return true;
                default:
                    return false;
            }
        });
    }



    // Set search menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void setFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_frame, fragment);
        fragmentTransaction.commit();
    }
}