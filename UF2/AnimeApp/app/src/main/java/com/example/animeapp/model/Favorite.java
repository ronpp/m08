package com.example.animeapp.model;

public class Favorite {

    private String animeid;
    private String name;
    private String imageurl;

    public Favorite(String animeid, String name, String imageurl) {
        this.animeid = animeid;
        this.name = name;
        this.imageurl = imageurl;
    }


    public String getAnimeid() {
        return animeid;
    }

    public void setAnimeid(String animeid) {
        this.animeid = animeid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }
}
