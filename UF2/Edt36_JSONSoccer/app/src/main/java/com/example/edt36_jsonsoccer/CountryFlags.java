package com.example.edt36_jsonsoccer;

import java.util.HashMap;
import java.util.Map;

public interface CountryFlags {

    Map<String, Integer> FLAGS = new HashMap<String, Integer>() {{
                put("argentina", R.drawable.argentina);
                put("australia", R.drawable.australia);
                put("canada", R.drawable.canada);
                put("congo", R.drawable.congo);
                put("egypt", R.drawable.egypt);
                put("france", R.drawable.france);
                put("germany", R.drawable.germany);
                put("greece", R.drawable.greece);
                put("india", R.drawable.india);
                put("indonesia", R.drawable.indonesia);
                put("ireland", R.drawable.ireland);
                put("italy", R.drawable.italy);
                put("kenya", R.drawable.kenya);
                put("mexico", R.drawable.mexico);
                put("newzealand", R.drawable.newzealand);
                put("norway", R.drawable.norway);
                put("poland", R.drawable.poland);
                put("qatar", R.drawable.qatar);
                put("southafrica", R.drawable.southafrica);
                put("southkorea", R.drawable.southkorea);
                put("spain", R.drawable.spain);
                put("uk", R.drawable.uk);
                put("usa", R.drawable.usa);
    }};

}
