package com.example.edt36_jsonsoccer;

import java.util.ArrayList;

public class CountryDetails {

    private String imgFlag;
    private String title;
    private String description;
    private String urlWeb;
    private ArrayList<String> images = new ArrayList<>(4);

    public CountryDetails() {
    }

    public CountryDetails(String imgFlag, String title, String description, String urlWeb, ArrayList<String> images) {
        this.imgFlag = imgFlag;
        this.title = title;
        this.description = description;
        this.urlWeb = urlWeb;
        this.images = images;
    }


    public String getImgFlag() {
        return imgFlag;
    }

    public void setImgFlag(String imgFlag) {
        this.imgFlag = imgFlag;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrlWeb() {
        return urlWeb;
    }

    public void setUrlWeb(String urlWeb) {
        this.urlWeb = urlWeb;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public int getArrayImagesSize(){
        return this.images.size();
    }

    @Override
    public String toString() {
        return "CountryDetails{" +
                "imgFlag='" + imgFlag + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", urlWeb='" + urlWeb + '\'' +
                '}';
    }
}
