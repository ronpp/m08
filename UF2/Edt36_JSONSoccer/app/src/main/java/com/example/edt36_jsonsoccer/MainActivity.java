package com.example.edt36_jsonsoccer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.edt36_jsonsoccer.adapters.CountryAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map.Entry;

public class MainActivity extends AppCompatActivity {

    public static String COUNTRY = "com.example.edt36_jsonsoccer.COUNTRY";

    private static String JSON_COUNTRIES_URL = "https://www.thesportsdb.com/api/v1/json/2/all_countries.php";
    private Spinner spnCountry;
    private ArrayList<Country> countries = new ArrayList<>();;
    private CountryAdapter countryAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Hook
        spnCountry = findViewById(R.id.spnCountry);

        getCountries();

    }


    private void getCountries(){
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                JSON_COUNTRIES_URL,
                null,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("countries");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                String countryName = ((JSONObject)jsonArray.get(i)).getString("name_en");
                                // remove all whitespace and convert to lowercase
                                countryName = countryName.toLowerCase().replaceAll("\\s", "");
                                for (Entry<String, Integer> entry: CountryFlags.FLAGS.entrySet()) {
                                    if(entry.getKey().equals(countryName)){
                                        countries.add(new Country(countryName, entry.getValue()));
                                    }
                                }
                            }
                            countryAdapter = new CountryAdapter(getApplicationContext(), countries);
                            spnCountry.setAdapter(countryAdapter);
                            spnCountry.setSelection(0, true);
                            spnCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    Country selectedItem = (Country)parent.getItemAtPosition(position);
                                    String selectedCountryName = selectedItem.getName();
                                     Intent i = new Intent(getApplicationContext(),  DetailActivity.class);
                                     i.putExtra(COUNTRY, selectedCountryName);
                                     Toast.makeText(MainActivity.this, selectedCountryName, Toast.LENGTH_SHORT).show();
                startActivity(i);
                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {
                                    Toast.makeText(MainActivity.this, "Nothing Selected", Toast.LENGTH_SHORT).show();

                                }
                            });

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("tag", "onErrorResponse: " + error.getMessage());
                    }
                }
        );
        requestQueue.add(jsonObjectRequest);
    }


}