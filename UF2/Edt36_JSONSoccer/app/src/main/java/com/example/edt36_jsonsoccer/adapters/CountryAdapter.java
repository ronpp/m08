package com.example.edt36_jsonsoccer.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.edt36_jsonsoccer.Country;
import com.example.edt36_jsonsoccer.R;

import java.util.ArrayList;


public class CountryAdapter extends ArrayAdapter<Country> {


    public CountryAdapter(@NonNull Context context, ArrayList<Country> countries) {
        super(context, 0 , countries);
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return init(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return init(position, convertView, parent);
    }

    public View init(int position, @Nullable View convertView, @NonNull ViewGroup parent){
        //layout
        if (convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.spinner_row,
                    parent,
                    false
            );
        }
        // Hook
        ImageView imgCountry = convertView.findViewById(R.id.imgCountry);
        TextView countryName = convertView.findViewById(R.id.countryName);

        // setText and setImageResource
        Country currentItem = getItem(position);

        if (currentItem != null){
            imgCountry.setImageResource(currentItem.getImgFlag());
            countryName.setText(currentItem.getName());
        }
        return convertView;
    }
}
