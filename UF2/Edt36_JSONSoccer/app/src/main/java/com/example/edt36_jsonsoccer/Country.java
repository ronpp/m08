package com.example.edt36_jsonsoccer;

import java.util.ArrayList;
import java.util.List;

public class Country {

    private String name;
    private int imgFlag;


    public Country(String name, int imgFlag) {
        this.name = name;
        this.imgFlag = imgFlag;
    }

    public String getName() {
        return name;
    }
    public int getImgFlag() {
        return imgFlag;
    }


}
