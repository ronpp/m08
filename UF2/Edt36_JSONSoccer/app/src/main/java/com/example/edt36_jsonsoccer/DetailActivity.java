package com.example.edt36_jsonsoccer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.edt36_jsonsoccer.adapters.DetailsAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DetailActivity extends AppCompatActivity {


    private RecyclerView recyclerDetails;
    ArrayList<CountryDetails> detailsList = new ArrayList<>();
    String JSON_COUNTRY_URL = "";
    DetailsAdapter detailsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        // Hook
        recyclerDetails = findViewById(R.id.recycleDetails);

        String selectedCountryName = getIntent().getStringExtra(MainActivity.COUNTRY);
        String JSON_COUNTRY_URL = String.format("https://www.thesportsdb.com/api/v1/json/2/search_all_leagues.php?c=%s&s=Soccer",selectedCountryName) ;
        // Toast Test
        Toast.makeText(this, selectedCountryName, Toast.LENGTH_SHORT).show();

        // Get data from JSON
        getCountryDetails(JSON_COUNTRY_URL);

    }


    private void getCountryDetails(String JSON_COUNTRY_URL){

        RequestQueue requestQueue =  Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                JSON_COUNTRY_URL,
                null,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("countrys");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = ((JSONObject)jsonArray.get(i));
                                CountryDetails countryDetails = new CountryDetails();
                                countryDetails.setImgFlag(jsonObject.getString("strBadge"));
                                countryDetails.setTitle(jsonObject.getString("strLeague"));
                                countryDetails.setDescription(jsonObject.getString("strDescriptionEN"));
                                countryDetails.setUrlWeb(jsonObject.getString("strWebsite"));
                                if (!"null".equals(jsonObject.getString("strFanart1")) ) {
                                    countryDetails.getImages().add(jsonObject.getString("strFanart1"));
                                    countryDetails.getImages().add(jsonObject.getString("strFanart2"));
                                    countryDetails.getImages().add(jsonObject.getString("strFanart3"));
                                    countryDetails.getImages().add(jsonObject.getString("strFanart4"));
                                }
                                detailsList.add(countryDetails);
                            }
                           
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        recyclerDetails.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        detailsAdapter = new DetailsAdapter(getApplicationContext(), detailsList);
                        recyclerDetails.setAdapter(detailsAdapter);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("tag", "onErrorResponse: " + error.getMessage());
                    }
                }
        );



        requestQueue.add(jsonObjectRequest);
    }

}