package com.example.edt36_jsonsoccer.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.edt36_jsonsoccer.CountryDetails;
import com.example.edt36_jsonsoccer.Images;
import com.example.edt36_jsonsoccer.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class DetailsAdapter extends RecyclerView.Adapter<DetailsAdapter.MyViewHolder> {
   private Context context;
   private List<CountryDetails> countryDetails;

    public DetailsAdapter(Context context, List<CountryDetails> countryDetails) {
        this.context = context;
        this.countryDetails = countryDetails;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.details_layout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        int myPos = position;
        holder.txtTitle.setText(countryDetails.get(position).getTitle());
        holder.txtDescription.setText(countryDetails.get(position).getDescription());
        Picasso.get().load(countryDetails.get(position).getImgFlag())
                .fit()
                .centerCrop()
                .into(holder.imgFlag);

        // Disable  image's button and hide it
        holder.btnImg.setAlpha(0);
        holder.btnImg.setClickable(false);

        // Checking if there are images
        if (countryDetails.get(position).getArrayImagesSize() != 0) {
            holder.btnImg.setAlpha(1);
            holder.btnImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(context.getApplicationContext(), Images.class);
                    i.putStringArrayListExtra("imageUrls", countryDetails.get(myPos).getImages());
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                    context.startActivity(i);
                }
            });
        }


        holder.btnUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = countryDetails.get(myPos).getUrlWeb();
                // we Added "https://" to url, otherwise the app crashed
                url = url.startsWith("https://")? url : "https://"+ url;
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return countryDetails.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        ImageView imgFlag;
        TextView txtTitle;
        TextView txtDescription;
        Button btnUrl;
        Button btnImg;



        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imgFlag = itemView.findViewById(R.id.imgFlag);
            txtTitle = itemView.findViewById(R.id.txtTitle);
            txtDescription = itemView.findViewById(R.id.txtDescription);
            btnUrl = itemView.findViewById(R.id.btnUrl);
            btnImg = itemView.findViewById(R.id.btnImg);
        }
    }
}
