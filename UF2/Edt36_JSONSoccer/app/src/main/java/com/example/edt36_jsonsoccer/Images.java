package com.example.edt36_jsonsoccer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.example.edt36_jsonsoccer.adapters.ImageAdapter;

import java.util.ArrayList;

public class Images extends AppCompatActivity {

    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_images);


        // Hook
        viewPager = findViewById(R.id.viewPager);
        ArrayList<String> imagesUrls = getIntent().getStringArrayListExtra("imageUrls");
        ImageAdapter imageAdapter = new ImageAdapter(this, imagesUrls);
        viewPager.setAdapter(imageAdapter);
    }
}