package com.example.newtimeapp.Fragments;

import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.newtimeapp.R;
import com.example.newtimeapp.adapters.ResultAdapter;
import com.example.newtimeapp.adapters.TopResultAdapter;
import com.example.newtimeapp.model.TopResult;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class TopStoryFragment extends Fragment {

    private List<String> stories = new ArrayList<>(Arrays.asList(
            "Select a story", "arts", "automobiles", "books", "business", "fashion", "food", "health",
            "home", "insider", "magazine", "movies", "nyregion", "obituaries", "opinion", "politics",
            "realestate", "science", "sports", "sundayreview", "technology", "theater", "t-magazine",
            "travel", "upshot", "us", "world" ));


    private TextView txtStatus;
    private TextView txtResultNum;
    private RecyclerView recyclerStories;

    public TopStoryFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_top_story, container, false);

        // Hook
        Spinner spnStories = view.findViewById(R.id.spnStories);
        txtStatus = view.findViewById(R.id.txtStoryStatus);
        txtResultNum = view.findViewById(R.id.txtStoryResultNum);
        recyclerStories = view.findViewById(R.id.recyclerStories);

        // Spinner Adapter
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                getContext(),
                 com.google.android.material.R.layout.support_simple_spinner_dropdown_item,
                stories);
        spnStories.setAdapter(arrayAdapter);

        spnStories.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(parent.getChildAt(0) != null){
                    ((TextView)parent.getChildAt(0)).setTextColor(Color.WHITE);
                }
                String selectedOption = parent.getSelectedItem().toString();
                //Toast.makeText(getContext(), selectedOption, Toast.LENGTH_SHORT).show();
                getData(selectedOption);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getContext(), "Nothing Selected", Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

    private void getData(String story){
        // Url request
        String token = "JjgN1ztBIy3aPxa5ZoOBRj0G6Gp6MX5Y";
        String url = "https://api.nytimes.com/svc/topstories/v2/" + story + ".json" + "?api-key=" + token;
        // Request
        RequestQueue requestQueue =  Volley.newRequestQueue(getContext());
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                url,
                null,
                response -> {
                    String status = response.optString("status");
                    int numResults = response.optInt("num_results");
                    txtStatus.setText(status);
                    txtResultNum.setText(String.valueOf(numResults));
                    if(numResults !=0){
                        JSONArray jsonArray = response.optJSONArray("results");
                        TopResult[] topResults = new GsonBuilder().create().fromJson(jsonArray.toString(),
                                TopResult[].class);
                        TopResultAdapter topResultAdapter = new TopResultAdapter(getContext(), topResults);
                        recyclerStories.setAdapter(topResultAdapter);
                        recyclerStories.setLayoutManager(new LinearLayoutManager(getContext()));
                    }
                },
                error -> { }
        );
        requestQueue.add(request);
    }
}