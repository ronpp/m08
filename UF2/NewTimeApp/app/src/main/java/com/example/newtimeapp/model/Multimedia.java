package com.example.newtimeapp.model;

import java.io.Serializable;

public class Multimedia implements Serializable {

    private String type;
    private String src;
    private String width;
    private String height;

    public Multimedia(String type, String src, String width, String height) {
        this.type = type;
        this.src = src;
        this.width = width;
        this.height = height;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }
}
