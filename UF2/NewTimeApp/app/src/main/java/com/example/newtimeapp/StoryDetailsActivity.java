package com.example.newtimeapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.newtimeapp.model.Result;
import com.example.newtimeapp.model.TopResult;
import com.squareup.picasso.Picasso;

public class StoryDetailsActivity extends AppCompatActivity {

    private ImageView imgDetailsStory;
    private TextView txtDStoryTitle;
    private TextView txtDStoryByline;
    private TextView txtDStoryType;
    private TextView txtDStoryDate;
    private TextView txtDStoryAbstract;
    private Button btnStoryUrl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story_details);

        // Hook
        imgDetailsStory = findViewById(R.id.imgDetailsStory);
        txtDStoryTitle = findViewById(R.id.txtDStoryTitle);
        txtDStoryByline = findViewById(R.id.txtDStoryByline);
        txtDStoryType = findViewById(R.id.txtDStoryType);
        txtDStoryDate = findViewById(R.id.txtDStoryDate);
        txtDStoryAbstract = findViewById(R.id.txtDStoryAbstract);
        btnStoryUrl = findViewById(R.id.btnStoryUrl);

        Bundle bundle = getIntent().getExtras();
        TopResult topResult = (TopResult) bundle.getSerializable("result");
        if (topResult.getMultimedia() != null) {
            Picasso.get().load(topResult.getMultimedia()[0].getUrl())
                    .fit()
                    .centerCrop()
                    .into(imgDetailsStory);
        } else {
            imgDetailsStory.setImageResource(R.drawable.nytlogo1);
        }

        txtDStoryTitle.setText(topResult.getTitle());
        txtDStoryByline.setText(topResult.getByline());
        txtDStoryType.setText(topResult.getItem_type());
        txtDStoryDate.setText(topResult.getPublished_date());
        txtDStoryAbstract.setText(topResult.getAbstract1());
        btnStoryUrl.setOnClickListener(view -> {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(topResult.getUrl()));
            startActivity(i);
        });
    }
}