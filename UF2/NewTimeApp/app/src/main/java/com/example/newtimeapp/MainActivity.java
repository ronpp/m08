package com.example.newtimeapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.Button;
import android.widget.FrameLayout;

import com.example.newtimeapp.Fragments.NewsFragment;
import com.example.newtimeapp.Fragments.TopStoryFragment;

public class MainActivity extends AppCompatActivity {


  private Button btnNews;
  private Button btnTopStories;
  private FrameLayout main_frame;
  private NewsFragment newsFragment;
  private TopStoryFragment topStoryFragment;


  private void buttonAction(Button button1, Button button2){
      button1.setBackgroundColor(Color.BLACK);
      button1.setTextColor(Color.WHITE);
      button2.setBackgroundColor(Color.WHITE);
      button2.setTextColor(Color.BLACK);
  }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Hook
        main_frame = findViewById(R.id.main_frame);
        btnNews = findViewById(R.id.btnNews);
        btnTopStories = findViewById(R.id.btnTopStories);


        newsFragment = new NewsFragment();
        topStoryFragment = new TopStoryFragment();

        // Default Fragment
        setFragment(newsFragment);

        btnNews.setOnClickListener(v ->{
            buttonAction(btnNews, btnTopStories);
            setFragment(newsFragment);
        });

        btnTopStories.setOnClickListener(v ->{
            buttonAction(btnTopStories, btnNews);
            setFragment(topStoryFragment);
        });



    }

    private void setFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_frame, fragment);
        fragmentTransaction.commit();
    }
}