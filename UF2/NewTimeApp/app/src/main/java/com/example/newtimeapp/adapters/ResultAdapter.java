package com.example.newtimeapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.newtimeapp.DetailsActivity;
import com.example.newtimeapp.R;
import com.example.newtimeapp.model.Result;
import com.squareup.picasso.Picasso;

public class ResultAdapter extends RecyclerView.Adapter<ResultAdapter.MyViewHolder> {

    private Context context;
    private Result[] results;

    public ResultAdapter(Context context, Result[] results) {
        this.context = context;
        this.results = results;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.news_layout, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.title.setText(results[position].getDisplay_title());
        holder.byline.setText(results[position].getByline());
        holder.headline.setText(results[position].getHeadline());
        holder.publidate.setText(results[position].getPublication_date());

        // Check multimedia
       if(results[position].getMultimedia() != null){
           Picasso.get().load(results[position].getMultimedia().getSrc())
                   .fit()
                   .centerCrop()
                   .into(holder.imgNews);
       }else {
          holder.imgNews.setImageResource(R.drawable.nytlogo1);
       }

       holder.newsLayout.setOnClickListener(view -> {
           Intent intent = new Intent(context, DetailsActivity.class);
           Bundle bundle = new Bundle();
           bundle.putSerializable("result", results[position]);
           intent.putExtras(bundle);
           context.startActivity(intent);
       });

    }

    @Override
    public int getItemCount() {
        return results.length;
    }


    public  class MyViewHolder extends RecyclerView.ViewHolder{
        TextView title;
        TextView byline;
        TextView headline;
        TextView publidate;
        ImageView imgNews;
        ConstraintLayout newsLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.txtTitle);
            byline = itemView.findViewById(R.id.txtByline);
            headline = itemView.findViewById(R.id.txtHeadline);
            publidate = itemView.findViewById(R.id.txtPublidate);
            imgNews = itemView.findViewById(R.id.imgNews);
            newsLayout = itemView.findViewById(R.id.newsLayout);
        }
    }
}
