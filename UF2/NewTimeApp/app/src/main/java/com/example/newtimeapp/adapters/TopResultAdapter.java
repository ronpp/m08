package com.example.newtimeapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.newtimeapp.DetailsActivity;
import com.example.newtimeapp.R;
import com.example.newtimeapp.StoryDetailsActivity;
import com.example.newtimeapp.model.TopResult;
import com.squareup.picasso.Picasso;


public class TopResultAdapter  extends RecyclerView.Adapter<TopResultAdapter.MyViewHolder>{

    private Context context;
    private TopResult[] topResults;

    public TopResultAdapter(Context context, TopResult[] topResults) {
        this.context = context;
        this.topResults = topResults;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.news_layout, parent, false);

        return new TopResultAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.title.setText(topResults[position].getTitle());
        holder.byline.setText(topResults[position].getByline());
        holder.headline.setText(topResults[position].getItem_type());
        holder.publidate.setText(topResults[position].getPublished_date());

        // Check multimedia
        if(topResults[position].getMultimedia() != null){
            Picasso.get().load(topResults[position].getMultimedia()[0].getUrl())
                    .fit()
                    .centerCrop()
                    .into(holder.imgNews);
        }else {
            holder.imgNews.setImageResource(R.drawable.nytlogo1);
        }

        holder.newsLayout.setOnClickListener(view -> {
            Intent intent = new Intent(context, StoryDetailsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("result", topResults[position]);
            intent.putExtras(bundle);
            context.startActivity(intent);
        });



    }

    @Override
    public int getItemCount() {
        return topResults.length;
    }


    public  class MyViewHolder extends RecyclerView.ViewHolder{
        TextView title;
        TextView byline;
        TextView headline;
        TextView publidate;
        ImageView imgNews;
        ConstraintLayout newsLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.txtTitle);
            byline = itemView.findViewById(R.id.txtByline);
            headline = itemView.findViewById(R.id.txtHeadline);
            publidate = itemView.findViewById(R.id.txtPublidate);
            imgNews = itemView.findViewById(R.id.imgNews);
            newsLayout = itemView.findViewById(R.id.newsLayout);
        }
    }
}
