package com.example.newtimeapp.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.newtimeapp.R;
import com.example.newtimeapp.adapters.ResultAdapter;
import com.example.newtimeapp.model.Result;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;


public class NewsFragment extends Fragment {


    public NewsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news, container, false);

        // Hook
        TextInputEditText txtSearch = view.findViewById(R.id.txtSearch);
        TextView txtStatus = view.findViewById(R.id.txtStatus);
        TextView txtResultNum = view.findViewById(R.id.txtResultNum);
        RecyclerView recyclerNews = view.findViewById(R.id.recyclerStories);
        ImageView imgSearch = view.findViewById(R.id.imgSearch);

        imgSearch.setOnClickListener(v -> {
            // Url request
            String userInput = txtSearch.getEditableText().toString().trim();
            String token = "JjgN1ztBIy3aPxa5ZoOBRj0G6Gp6MX5Y";
            String url = "https://api.nytimes.com/svc/movies/v2/reviews/search.json?query=" + userInput + "&api-key=" + token;

            // Request
            RequestQueue requestQueue =  Volley.newRequestQueue(getContext());
            JsonObjectRequest request = new JsonObjectRequest(
                    Request.Method.GET,
                    url,
                    null,
                    response -> {
                        String status = response.optString("status");
                        int numResults = response.optInt("num_results");
                        txtStatus.setText(status);
                        txtResultNum.setText(String.valueOf(numResults));
                        if(numResults !=0){
                            JSONArray jsonArray = response.optJSONArray("results");
                            Result[] results = new GsonBuilder().create().fromJson(jsonArray.toString(),
                                    Result[].class);
                            ResultAdapter resultAdapter = new ResultAdapter(getContext(), results);
                            recyclerNews.setAdapter(resultAdapter);
                            recyclerNews.setLayoutManager(new LinearLayoutManager(getContext()));
                        }
                    },
                    error -> {
                        Toast.makeText(getContext(),"Something Wrong", Toast.LENGTH_SHORT).show();
                    }
            );
            requestQueue.add(request);
        });

        return view;
    }
}