package com.example.newtimeapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.newtimeapp.model.Result;
import com.squareup.picasso.Picasso;

public class DetailsActivity extends AppCompatActivity {

    private ImageView imgDetailsNews;
    private TextView txtDetailsTitle;
    private TextView txtDetailsByline;
    private TextView txtDetailsHeadline;
    private TextView txtDetailsDate;
    private TextView txtDetailsSummary;
    private Button btnUrl;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        // Hook
        imgDetailsNews = findViewById(R.id.imgDetailsStory);
        txtDetailsTitle = findViewById(R.id.txtDStoryTitle);
        txtDetailsByline = findViewById(R.id.txtDStoryByline);
        txtDetailsHeadline = findViewById(R.id.txtDStoryType);
        txtDetailsDate = findViewById(R.id.txtDStoryDate);
        txtDetailsSummary = findViewById(R.id.txtDStoryAbstract);
        btnUrl = findViewById(R.id.btnStoryUrl);

        Bundle bundle = getIntent().getExtras();
        Result result = (Result) bundle.getSerializable("result");

        if (result.getMultimedia() != null) {
            Picasso.get().load(result.getMultimedia().getSrc())
                    .fit()
                    .centerCrop()
                    .into(imgDetailsNews);
        } else {
            imgDetailsNews.setImageResource(R.drawable.nytlogo1);
        }
        txtDetailsTitle.setText(result.getDisplay_title());
        txtDetailsByline.setText(result.getByline());
        txtDetailsHeadline.setText(result.getHeadline());
        txtDetailsDate.setText(result.getPublication_date());
        txtDetailsSummary.setText(result.getSummary_short());
        
        if(result.getLink() != null){
            btnUrl.setOnClickListener(view -> {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(result.getLink().getUrl()));
                startActivity(i);
            });
        } else {
            Toast.makeText(this, "THERE IS NO LINK", Toast.LENGTH_SHORT).show();
        }

    }
}