package com.example.edt23_viewpageimage;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;


import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    private int [] imagesIds = new int[]{
            R.drawable.botanical_garden,
            R.drawable.jaragua_park,
            R.drawable.los_haitises_park,
            R.drawable.ozama_wetlands,
            R.drawable.three_eyes
    };

    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = findViewById(R.id.viewPager);

        ImageAdapter imageAdapter = new ImageAdapter(this, imagesIds);
        viewPager.setAdapter(imageAdapter);
    }
}