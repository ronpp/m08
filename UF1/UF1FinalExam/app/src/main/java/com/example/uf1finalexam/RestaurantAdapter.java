package com.example.uf1finalexam;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class RestaurantAdapter extends RecyclerView.Adapter<RestaurantAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<Restaurant> restaurants;

    public RestaurantAdapter(Context context, ArrayList<Restaurant> restaurants) {
        this.context = context;
        this.restaurants = restaurants;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.restaurant_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        int pos = position;
        holder.txtNameValue.setText(restaurants.get(position).getItemName());
        holder.txtTypeValue.setText(restaurants.get(position).getItemType());
        holder.txtRatingValue.setText(restaurants.get(position).getItemRating());
        holder.txtRangeValue.setText(restaurants.get(position).getItemRange());
        holder.imgRestaurant.setImageResource(restaurants.get(position).getItemImage());

        holder.restaurantLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String  itemName = restaurants.get(pos).getItemName();
                String  itemType = restaurants.get(pos).getItemType();
                String  itemRating = restaurants.get(pos).getItemRating();
                String  itemRange = restaurants.get(pos).getItemRange();
                String  itemCity = restaurants.get(pos).getItemCity();
                String  itemAddress = restaurants.get(pos).getItemAddress();
                String  itemTel = restaurants.get(pos).getItemTel();
                String itemDesc = restaurants.get(pos).getItemDescription();
                int itemImg = restaurants.get(pos).getItemImage();
                ArrayList<Integer> itemImages = restaurants.get(pos).getItemImages();
                RestaurantDetailFragment restaurantDetailFragment = RestaurantDetailFragment.newInstance(
                        itemName,
                        itemType,
                        itemRating,
                        itemRange,
                        itemCity,
                        itemAddress,
                        itemTel,
                        itemDesc,
                        itemImg,
                        itemImages
                );
                FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.main_frame, restaurantDetailFragment);
                fragmentTransaction.commit();


            }
        });

    }

    @Override
    public int getItemCount() {
        return restaurants.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView txtNameValue;
        TextView txtTypeValue;
        TextView txtRatingValue;
        TextView txtRangeValue;
        ImageView imgRestaurant;
        ConstraintLayout restaurantLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtNameValue = itemView.findViewById(R.id.txtNameValue);
            txtTypeValue = itemView.findViewById(R.id.txtTypeValue);
            txtRatingValue = itemView.findViewById(R.id.txtRatingValue);
            txtRangeValue = itemView.findViewById(R.id.txtRangeValue);
            imgRestaurant = itemView.findViewById(R.id.imgRestaurant);
            restaurantLayout = itemView.findViewById(R.id.restaurantLayout);
        }
    }
}
