package com.example.uf1finalexam;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private BottomNavigationView main_nav;
    private FrameLayout main_frame;
    private RestaurantFragment restaurantFragment;
    private SiteFragment siteFragment;
    private GuideFragment guideFragment;
    private ArrayList<Restaurant> restaurants;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Hook
        toolbar = findViewById(R.id.toolbar);
        main_nav = findViewById(R.id.main_nav);
        main_frame = findViewById(R.id.main_frame);

        // Set toolbar
        setSupportActionBar(toolbar);

        // Init Data
        initData();

        // Init Fragment
        restaurantFragment = RestaurantFragment.newInstance(restaurants);
        siteFragment = new SiteFragment();
        guideFragment = new GuideFragment();

        // Default Fragment
        setFragment(restaurantFragment);


        // Set Nav menu listener
        main_nav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.restaurant_icon:
                        setFragment(restaurantFragment);
                        main_nav.setItemBackgroundResource(R.color.restaurant_frame_color);
                        return true;
                    case R.id.site_icon:_icon:
                        setFragment(siteFragment);
                         main_nav.setItemBackgroundResource(R.color.site_frame_color);
                        return true;
                    case R.id.guide_icon:
                        setFragment(guideFragment);
                        main_nav.setItemBackgroundResource(R.color.guide_frame_color);
                        return true;
                    default:
                        return false;
                }

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.my_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.mail_icon:
                Toast.makeText(this, "MAIL", Toast.LENGTH_SHORT).show();
                return  true;
            case R.id.language_icon:
                Toast.makeText(this, "LANGUAGE", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.settings_icon:
                Toast.makeText(this, "SETTINGS", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_frame, fragment);
        fragmentTransaction.commit();
    }


    public void initData(){

        restaurants = new ArrayList<>();

        Restaurant item1 = new Restaurant("Restaurant 1", R.drawable.rest1,
                "This is an amazing Asian Food restaurant in the heart of Barcelona, in fornt of the marina street.",
                "4/5", "Asian", "€30 - €55", "Barcelona", "Maria St, 45", "+34 555 555 555",
                new ArrayList<Integer>(
                        Arrays.asList(R.drawable.rest1a, R.drawable.rest1b, R.drawable.rest1c)));
        Restaurant item2 = new Restaurant("Restaurant 2", R.drawable.rest2,
                "description Restaurant 2",
                "4/5", "Mediterranean", "€60 - €75", "Madrid", "Leon, 34", "+34 444 444 444",
                new ArrayList<Integer>(
                        Arrays.asList(R.drawable.rest2a, R.drawable.rest2b)));
        Restaurant item3 = new Restaurant("Restaurant 3", R.drawable.rest3,
                "description Restaurant 3",
                "4/5", "Italian", "€30 - €55", "Barcelona", "Maria St, 45", "+34 555 555 555",
                new ArrayList<Integer>(
                        Arrays.asList(R.drawable.rest3a)));
        Restaurant item4 = new Restaurant("Restaurant 4", R.drawable.rest4,
                "description Restaurant 4",
                "4/5", "Asian", "€30 - €55", "Barcelona", "Maria St, 45", "+34 555 555 555",
                new ArrayList<Integer>(
                        Arrays.asList(R.drawable.rest4a, R.drawable.rest4b)));
        Restaurant item5 = new Restaurant("Restaurant 5", R.drawable.rest5,
                "description Restaurant 5",
                "4/5", "Asian", "€30 - €55", "Barcelona", "Maria St, 45", "+34 555 555 555",
                new ArrayList<Integer>(
                        Arrays.asList(R.drawable.rest5a, R.drawable.rest5b)));
        Restaurant item6 = new Restaurant("Restaurant 6", R.drawable.rest6,
                "description Restaurant 6",
                "4/5", "Asian", "€30 - €55","Barcelona", "Maria St, 45", "+34 555 555 555",
                new ArrayList<Integer>(
                        Arrays.asList(R.drawable.rest6a, R.drawable.rest6b)));
        Restaurant item7 = new Restaurant("Restaurant 7", R.drawable.rest7,
                "description Restaurant 7",
                "4/5", "Asian", "€30 - €55", "Barcelona", "Maria St, 45", "+34 555 555 555",
                new ArrayList<Integer>(
                        Arrays.asList(R.drawable.rest7a, R.drawable.rest7b)));

        restaurants.add(item1);
        restaurants.add(item2);
        restaurants.add(item3);
        restaurants.add(item4);
        restaurants.add(item5);
        restaurants.add(item6);
        restaurants.add(item7);
    }

}