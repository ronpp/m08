package com.example.uf1finalexam;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Set;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RestaurantDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RestaurantDetailFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";
    private static final String ARG_PARAM5 = "param5";
    private static final String ARG_PARAM6 = "param6";
    private static final String ARG_PARAM7 = "param7";
    private static final String ARG_PARAM8 = "param8";
    private static final String ARG_PARAM9 = "param9";
    private static final String ARG_PARAM10 = "param10";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String mParam3;
    private String mParam4;
    private String mParam5;
    private String mParam6;
    private String mParam7;
    private String mParam8;
    private int mParam9;
    private ArrayList<Integer> mParam10;

    public RestaurantDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RestaurantDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RestaurantDetailFragment newInstance(String param1, String param2, String param3, String param4, String param5, String param6,
                                                       String param7, String param8, int param9, ArrayList<Integer> param10) {
        RestaurantDetailFragment fragment = new RestaurantDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, param3);
        args.putString(ARG_PARAM4, param4);
        args.putString(ARG_PARAM5, param5);
        args.putString(ARG_PARAM6, param6);
        args.putString(ARG_PARAM7, param7);
        args.putString(ARG_PARAM8, param8);
        args.putInt(ARG_PARAM9, param9);
        args.putSerializable(ARG_PARAM10, param10);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mParam3 = getArguments().getString(ARG_PARAM3);
            mParam4 = getArguments().getString(ARG_PARAM4);
            mParam5 = getArguments().getString(ARG_PARAM5);
            mParam6 = getArguments().getString(ARG_PARAM6);
            mParam7 = getArguments().getString(ARG_PARAM7);
            mParam8 = getArguments().getString(ARG_PARAM8);
            mParam9 = getArguments().getInt(ARG_PARAM9);
            mParam10 = (ArrayList<Integer>)getArguments().getSerializable(ARG_PARAM10);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_restaurant_detail, container, false);

        // Hook
        TextView txtNameDetail = view.findViewById(R.id.txtNameDetail);
        TextView txtTypeDetailValue = view.findViewById(R.id.txtTypeDetailValue);
        TextView txtRatingDetailValue = view.findViewById(R.id.txtRatingDetailValue);
        TextView txtRangeDetailValue = view.findViewById(R.id.txtRangeDetailValue);
        TextView txtCityDetailValue = view.findViewById(R.id.txtCityDetailValue);
        TextView txtAddressDetailValue = view.findViewById(R.id.txtAddresDetailValue);
        TextView txtTelDetailValue = view.findViewById(R.id.txtTelDetailsValue);
        TextView txtDescDetail = view.findViewById(R.id.txtDescDetail);
        ImageView imgDetail = view.findViewById(R.id.imgDetail);
        RecyclerView recyclerImgDetail = view.findViewById(R.id.recyclerImgDetail);

        // Set Data
        txtNameDetail.setText(mParam1);
        txtTypeDetailValue.setText(mParam2);
        txtRatingDetailValue.setText(mParam3);
        txtRangeDetailValue.setText(mParam4);
        txtCityDetailValue.setText(mParam5);
        txtAddressDetailValue.setText(mParam6);
        txtTelDetailValue.setText(mParam7);
        txtDescDetail.setText(mParam8);
        imgDetail.setImageResource(mParam9);

        ImageAdapter imageAdapter = new ImageAdapter(getContext(), mParam10);
        recyclerImgDetail.setAdapter(imageAdapter);
        recyclerImgDetail.setLayoutManager(new LinearLayoutManager(getContext()));



        return view;
    }
}