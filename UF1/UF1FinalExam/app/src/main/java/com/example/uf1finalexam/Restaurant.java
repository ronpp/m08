package com.example.uf1finalexam;

import java.util.ArrayList;

public class Restaurant {

    private String itemName;
    private int itemImage;
    private String itemDescription;
    private String itemRating;
    private String itemType;
    private String itemRange;
    private String itemCity;
    private String itemAddress;
    private String itemTel;
    private ArrayList<Integer> itemImages;


    public Restaurant(String itemName, int itemImage, String itemDescription, String itemRating,
                      String itemType, String itemRange, String itemCity, String itemAddress,
                      String itemTel, ArrayList<Integer> itemImages) {
        this.itemName = itemName;
        this.itemImage = itemImage;
        this.itemDescription = itemDescription;
        this.itemRating = itemRating;
        this.itemType = itemType;
        this.itemRange = itemRange;
        this.itemCity = itemCity;
        this.itemAddress = itemAddress;
        this.itemTel = itemTel;
        this.itemImages = itemImages;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getItemImage() {
        return itemImage;
    }

    public void setItemImage(int itemImage) {
        this.itemImage = itemImage;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getItemRating() {
        return itemRating;
    }

    public void setItemRating(String itemRating) {
        this.itemRating = itemRating;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getItemRange() {
        return itemRange;
    }

    public void setItemRange(String itemRange) {
        this.itemRange = itemRange;
    }

    public String getItemCity() {
        return itemCity;
    }

    public void setItemCity(String itemCity) {
        this.itemCity = itemCity;
    }

    public String getItemAddress() {
        return itemAddress;
    }

    public void setItemAddress(String itemAddress) {
        this.itemAddress = itemAddress;
    }

    public String getItemTel() {
        return itemTel;
    }

    public void setItemTel(String itemTel) {
        this.itemTel = itemTel;
    }

    public ArrayList<Integer> getItemImages() {
        return itemImages;
    }

    public void setItemImages(ArrayList<Integer> itemImages) {
        this.itemImages = itemImages;
    }
}
