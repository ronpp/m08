package com.example.animationsbikes;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final int SPLASH_SCREEN = 1500;

    private ImageView imgMainLogo;
    private ImageView imgLogoBack;
    private ImageView imgBack;
    private ImageView imgBike11;
    private ImageView imgBike12;
    private ImageView imgBike21;
    private ImageView imgBike22;

    private TextView txtCenterTitle;
    private TextView txtCenterSubtitle;

    private RelativeLayout containerCenterText;
    private RelativeLayout containerTopText;
    private TableLayout containerBikes;



    // Method to Bike Activity
    public void bikeActivity(ImageView imgBike) {
        //imgBike.animate().rotation(360).setDuration(1000);
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(imgBike, "rotation", 0, 360);
        objectAnimator.setDuration(1000);
        objectAnimator.start();
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(MainActivity.this, BikeActivity.class);
                startActivity(intent);
                overridePendingTransition(R.animator.slide_in_right, R.animator.slide_out_left);
            }
        }, SPLASH_SCREEN);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // Hook
        imgBack = findViewById(R.id.imgBack);
        imgMainLogo = findViewById(R.id.imgMainLogo);
        imgLogoBack = findViewById(R.id.imgLogoBack);
        imgBike11 = findViewById(R.id.imgBike11);
        imgBike12 = findViewById(R.id.imgBike12);
        imgBike21 = findViewById(R.id.imgBike21);
        imgBike22 = findViewById(R.id.imgBike22);
        txtCenterTitle = findViewById(R.id.txtCenterTitle);
        txtCenterSubtitle = findViewById(R.id.txtCenterSubtitle);
        containerTopText = findViewById(R.id.containerTopText);
        containerCenterText = findViewById(R.id.containerCenterText);
        containerBikes = findViewById(R.id.containerBikes);

        // Hide Elements
        imgLogoBack.setAlpha(0f);
        containerTopText.setAlpha(0f);
        containerBikes.setAlpha(0f);


        // Logos and containers animations
        imgMainLogo.animate().translationX(-500f).setDuration(800).setStartDelay(2000);
        containerCenterText.animate().translationY(-700f)
                .translationX(-400f)
                .setDuration(800)
                .alpha(0f)
                .setStartDelay(2000);

        imgBack.animate().translationY(-1500f).setDuration(1000).setStartDelay(2000);
        imgLogoBack.animate().translationX(-80f).setDuration(900).alpha(1f).setStartDelay(2000);
        containerTopText.animate().rotation(360f).translationX(50)
                .alpha(1f).
                setDuration(1000)
                .setStartDelay(2500);

        containerBikes.animate().alpha(1).translationY(-100).setDuration(1000).setStartDelay(3000);

        // Bike animations
        imgBike11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bikeActivity(imgBike11);
            }
        });

        imgBike12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bikeActivity(imgBike12);
            }
        });

        imgBike21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bikeActivity(imgBike21);
            }
        });

        imgBike22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bikeActivity(imgBike22);
            }
        });

    }
}