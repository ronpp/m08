package com.example.animationsbikes;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;

public class BikeActivity extends AppCompatActivity {
    ImageView imgBikeBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bike);

        //Hook
        imgBikeBack = findViewById(R.id.imgBikeBack);
        imgBikeBack.setTranslationY(-1500 );
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.animator.slide_in_left, R.animator.slide_out_right);
    }
}