package com.example.edt72;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    public static String ANIMAL_TITLE = "com.example.edt72.ANIMAL_TITLE";
    public static String ANIMAL_IMG = "com.example.edt72.ANIMAL_IMG";

    private CardView cardView11;
    private CardView cardView12;
    private CardView cardView21;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hook
        cardView11 = findViewById(R.id.cardView11);
        cardView12 = findViewById(R.id.cardView12);
        cardView21 = findViewById(R.id.cardView21);

        cardView11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String animalTitle = getString(R.string.txtCardView11);
                int animalImg = R.drawable.animals1;
                Intent intent = new Intent(MainActivity.this, activity_card_view.class);
                intent.putExtra(ANIMAL_TITLE, animalTitle);
                intent.putExtra(ANIMAL_IMG, animalImg);
                startActivity(intent);
            }
        });

        cardView12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String animalTitle = getString(R.string.txtCardView12);
                int animalImg = R.drawable.animals2;
                Intent intent = new Intent(MainActivity.this, activity_card_view.class);
                intent.putExtra(ANIMAL_TITLE, animalTitle);
                intent.putExtra(ANIMAL_IMG, animalImg);
                startActivity(intent);
            }
        });

        cardView21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String animalTitle = getString(R.string.txtCardView21);
                int animalImg = R.drawable.animals3;
                Intent intent = new Intent(MainActivity.this, activity_card_view.class);
                intent.putExtra(ANIMAL_TITLE, animalTitle);
                intent.putExtra(ANIMAL_IMG, animalImg);
                startActivity(intent);
            }
        });
    }
}