package com.example.edt72;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class activity_card_view extends AppCompatActivity {
    private TextView txtCardTitle;
    private ImageView imgCardLogo;
    private ImageView imgCardAnimal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_view);

        //hook
        txtCardTitle = findViewById(R.id.txtCardTitle);
        imgCardLogo = findViewById(R.id.imgCardLogo);
        imgCardAnimal = findViewById(R.id.imgCardAnimal);

        Intent intent = getIntent();
        String animalTitle = intent.getStringExtra(MainActivity.ANIMAL_TITLE);
        int animaImg = intent.getIntExtra(MainActivity.ANIMAL_IMG, 0);
        txtCardTitle.setText(animalTitle);
        imgCardLogo.setImageResource(animaImg);
        imgCardAnimal.setImageResource(animaImg);

    }
}