package com.example.edt48_recycleviewclass;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerPics;
    ArrayList<Picture> pictures = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Hook
        recyclerPics = findViewById(R.id.recyclePics);


        initData();
        PictureAdapter pictureAdapter = new PictureAdapter(this, pictures);
        recyclerPics.setAdapter(pictureAdapter);
        recyclerPics.setLayoutManager(new GridLayoutManager(this, 3, RecyclerView.VERTICAL, false));
    }

    private void initData(){

        for (int i = 1; i <= 38; i++) {
            Picture picture = new Picture();
            String url = "" ;
            if (i < 10){
                url = String.format("https://joanseculi.com/images/img0%d.jpg", i);
            } else {
                url = String.format("https://joanseculi.com/images/img%d.jpg", i);
            }
            picture.setUrl(url);
            picture.setText("Pic" + i);
            picture.setDesc("This is the description of de pic number " + i);
            pictures.add(picture);
        }

    }
}