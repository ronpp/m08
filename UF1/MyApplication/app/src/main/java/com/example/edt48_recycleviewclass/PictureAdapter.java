package com.example.edt48_recycleviewclass;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PictureAdapter extends RecyclerView.Adapter<PictureAdapter.MyViewHolder> {

    Context context;
    ArrayList<Picture> pictures;

    public PictureAdapter(Context context, ArrayList<Picture> pictures) {
        this.context = context;
        this.pictures = pictures;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.pic_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.txtText.setText(pictures.get(position).getText());
        Picasso.get()
                .load(pictures.get(position).getUrl())
                .fit()
                .centerCrop()
                .into(holder.imgPic);

        holder.rowLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = holder.getAdapterPosition();
                Intent i = new Intent(context, DetailActivity.class);
                i.putExtra("text", pictures.get(pos).getText());
                i.putExtra("desc", pictures.get(pos).getDesc());
                i.putExtra("url", pictures.get(pos).getUrl());
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return pictures.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView txtText;
        private ImageView imgPic;

        ConstraintLayout rowLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtText = itemView.findViewById(R.id.txtText);
            imgPic = itemView.findViewById(R.id.imgPic);
            rowLayout = itemView.findViewById(R.id.rowLayout);
        }
    }
}
