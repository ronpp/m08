package com.example.edt48_recycleviewclass;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity {

    private TextView textDetail;
    private TextView descDetail;
    private ImageView imgDetail;
    private String text, desc, url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        // Hook
        textDetail = findViewById(R.id.textDetail);
        descDetail = findViewById(R.id.descDetail);
        imgDetail = findViewById(R.id.imgDetail);

        getData();
        setData();
    }

    public void getData(){
        Intent i = getIntent();
        text = i.getStringExtra("text");
        desc = i.getStringExtra("desc");
        url = i.getStringExtra("url");
    }

    public void setData(){
        textDetail.setText(text);
        descDetail.setText(desc);
        Picasso.get()
                .load(url)
                .fit()
                .centerCrop()
                .into(imgDetail);
    }
}