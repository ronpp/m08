package com.example.edt29_parks;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Calendar;

public class ReservarActivity extends AppCompatActivity {


    private EditText txtEntrance;
    private EditText txtExit;

     DatePickerDialog setDate(EditText txtDate){
        DatePickerDialog picker;
        // Select actual Date
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        picker = new DatePickerDialog(ReservarActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                        // Show selected date
                        month ++; // Cause month is 0-11
                        txtDate.setText(dayOfMonth + "/" + month + "/" + year );
                    }
                }, year, month , day);
        return picker;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservar);

        // Hook
        txtEntrance = findViewById(R.id.txtEntrance);
        txtExit = findViewById(R.id.txtExit);

        txtEntrance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDate(txtEntrance).show();
            }
        });

        txtExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDate(txtExit).show();
            }
        });

    }
}