package com.example.edt29_parks;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {

    private static final int SPLASH_SCREEN = 500;

    private ImageView image11;
    private ImageView image12;
    private ImageView image2;
    private ImageView image31;
    private ImageView image32;

    private LinearLayout linear11;
    private LinearLayout linear12;
    private LinearLayout linear2;
    private LinearLayout linear31;
    private LinearLayout linear32;

    private Button btnReserva11;
    private Button btnReserva12;
    private Button btnReserva2;
    private Button btnReserva31;
    private Button btnReserva32;

    private boolean btn11IsClicked = false;
    private boolean btn12IsClicked = false;
    private boolean btn2IsClicked = false;
    private boolean btn31IsClicked = false;
    private boolean btn32IsClicked = false;

    ObjectAnimator objectAnimator1;
    ObjectAnimator objectAnimator2;
    ObjectAnimator objectAnimator3;
    ObjectAnimator objectAnimator4;

    // Method to going to Reservation Page
    public void reservationForm(View view) {
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(MainActivity.this, ReservarActivity.class);
                startActivity(intent);
            }
        }, SPLASH_SCREEN);
    }

    void setFrontAnimator(boolean btnIsClicked, Button btnReserva, ImageView image, LinearLayout linear) {
        if(!btnIsClicked) {
            objectAnimator1 = ObjectAnimator.ofFloat(image, "RotationY", 0, 180);
            objectAnimator1.setDuration(1000);
            objectAnimator2 = ObjectAnimator.ofFloat(image, "alpha", 1f, 0.5f);
            objectAnimator2.setStartDelay(500);
            objectAnimator2.setDuration(1);
            objectAnimator3 = ObjectAnimator.ofFloat(linear, "RotationY", -180, 0);
            objectAnimator3.setDuration(1000);
            objectAnimator4 = ObjectAnimator.ofFloat(linear, "alpha", 0f, 1f);
            objectAnimator4.setStartDelay(500);
            objectAnimator4.setDuration(1);
            btnReserva.setVisibility(View.VISIBLE);
        }
    }

    void  setBackAnimator(boolean btnIsClicked, Button btnReserva, ImageView image, LinearLayout linear){
        if(btnIsClicked){
            objectAnimator1 = ObjectAnimator.ofFloat(image, "RotationY", 180, 0);
            objectAnimator1.setDuration(1000);
            objectAnimator2 = ObjectAnimator.ofFloat(image, "alpha", 0.5f, 1f);
            objectAnimator2.setStartDelay(500);
            objectAnimator2.setDuration(1);
            objectAnimator3 = ObjectAnimator.ofFloat(linear, "RotationY", 0, -180);
            objectAnimator3.setDuration(1000);
            objectAnimator4 = ObjectAnimator.ofFloat(linear, "alpha", 1f, 0f);
            objectAnimator4.setStartDelay(500);
            objectAnimator4.setDuration(1);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    btnReserva.setVisibility(View.GONE);
                }
            }, SPLASH_SCREEN);
        }

    }

    void  playAnimator(){
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objectAnimator1, objectAnimator2, objectAnimator3, objectAnimator4);
        animatorSet.start();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

                                                                     // HOOK
        // Images
        image11 = findViewById(R.id.image11);
        image12 = findViewById(R.id.image12);
        image2 = findViewById(R.id.image2);
        image31 = findViewById(R.id.image31);
        image32 = findViewById(R.id.image32);

        // Linears
        linear11 = findViewById(R.id.linear11);
        linear12 = findViewById(R.id.linear12);
        linear2 = findViewById(R.id.linear2);
        linear31 = findViewById(R.id.linear31);
        linear32 = findViewById(R.id.linear32);

        // Buttons
        btnReserva11 = findViewById(R.id.btnReserva11);
        btnReserva12 = findViewById(R.id.btnReserva12);
        btnReserva2 = findViewById(R.id.btnReserva2);
        btnReserva31 = findViewById(R.id.btnReserva31);
        btnReserva32 = findViewById(R.id.btnReserva32);

        // Buttons and Texts Visibility
        btnReserva11.setVisibility(View.GONE);
        btnReserva12.setVisibility(View.GONE);
        btnReserva2.setVisibility(View.GONE);
        btnReserva31.setVisibility(View.GONE);
        btnReserva32.setVisibility(View.GONE);
        linear11.setAlpha(0);
        linear12.setAlpha(0);
        linear2.setAlpha(0);
        linear31.setAlpha(0);
        linear32.setAlpha(0);

        // Actions
        image11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!btn11IsClicked){
                    setFrontAnimator(btn11IsClicked, btnReserva11, image11, linear11);
                    btn11IsClicked = true;
                } else {
                    setBackAnimator(btn11IsClicked, btnReserva11, image11, linear11);
                    btn11IsClicked = false;
                }
                playAnimator();
            }
        });

        image12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!btn12IsClicked){
                    setFrontAnimator(btn12IsClicked, btnReserva12, image12, linear12);
                    btn12IsClicked = true;
                } else{
                    setBackAnimator(btn12IsClicked, btnReserva12, image12, linear12);
                    btn12IsClicked = false;
                }
                playAnimator();
            }

        });

        image2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!btn2IsClicked){
                    setFrontAnimator(btn2IsClicked, btnReserva2, image2, linear2);
                    btn2IsClicked = true;
                } else{
                    setBackAnimator(btn2IsClicked, btnReserva2, image2, linear2);
                    btn2IsClicked = false;
                }
                playAnimator();
            }

        });

        image31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!btn31IsClicked){
                    setFrontAnimator(btn31IsClicked, btnReserva31, image31, linear31);
                    btn31IsClicked = true;
                } else{
                    setBackAnimator(btn31IsClicked, btnReserva31, image31, linear31);
                    btn31IsClicked = false;
                }
                playAnimator();
            }

        });

        image32.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!btn32IsClicked){
                    setFrontAnimator(btn32IsClicked, btnReserva32, image32, linear32);
                    btn32IsClicked = true;
                } else{
                    setBackAnimator(btn32IsClicked, btnReserva32, image32, linear32);
                    btn32IsClicked = false;
                }
                playAnimator();
            }

        });
    }
}