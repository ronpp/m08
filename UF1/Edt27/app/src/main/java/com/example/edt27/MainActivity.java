package com.example.edt27;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private ImageView imageView1;
    private ImageView imageView11;
    private boolean clicked = false;
    ObjectAnimator objectAnimator1;
    ObjectAnimator objectAnimator2;
    ObjectAnimator objectAnimator3;
    ObjectAnimator objectAnimator4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hook
        imageView1 = findViewById(R.id.imageView1);
        imageView11 = findViewById(R.id.imageView11);
      /*  imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animator set;
               if(!clicked){
                   set = AnimatorInflater.loadAnimator(MainActivity.this, R.animator.frontanimator);
                   set.setTarget(v);
                   set.start();
                   clicked = true;
               } else{
                   set = AnimatorInflater.loadAnimator(MainActivity.this, R.animator.backanimator);
                   set.setTarget(v);
                   set.start();
                   clicked = false;
               }
            }
        });*/

        imageView11.setAlpha(0f);
        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!clicked){
                    objectAnimator1 = ObjectAnimator.ofFloat(v, "rotationY", 0f, 180f);
                    objectAnimator2 = ObjectAnimator.ofFloat(v,"alpha", 1f, 0f);
                    objectAnimator3 =  ObjectAnimator.ofFloat(imageView11,"rotationY", 180f, 0f);
                    objectAnimator4 =  ObjectAnimator.ofFloat(imageView11,"alpha", 0f, 1f);

                    objectAnimator1.setDuration(1000);
                    objectAnimator2.setDuration(1000);
                    objectAnimator3.setDuration(1000);
                    objectAnimator4.setDuration(1000);
                    objectAnimator4.setStartDelay(500);

                    AnimatorSet set = new AnimatorSet();
                    set.playTogether(objectAnimator1, objectAnimator2, objectAnimator3,objectAnimator4);
                    set.start();
                    clicked = true;
                }else{
                    objectAnimator1 = ObjectAnimator.ofFloat(v, "rotationY", 180f, 0f);
                    objectAnimator2 = ObjectAnimator.ofFloat(v,"alpha", 0f, 1f);
                    objectAnimator2.setStartDelay(500);
                    objectAnimator3 =  ObjectAnimator.ofFloat(imageView11,"rotationY", 0f, 180f);
                    objectAnimator4 =  ObjectAnimator.ofFloat(imageView11,"alpha", 1f, 0f);

                    objectAnimator1.setDuration(1000);
                    objectAnimator2.setDuration(1000);
                    objectAnimator3.setDuration(1000);
                    objectAnimator4.setDuration(1000);

                    AnimatorSet set = new AnimatorSet();
                    set.playTogether(objectAnimator1, objectAnimator2, objectAnimator3,objectAnimator4);
                    set.start();
                    clicked = false;
                }
            }
        });

    }
}