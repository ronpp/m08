package com.example.examen_uf1_edt76;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CdActivity extends AppCompatActivity {

    private ImageView imgCD;
    private TextView nameCD;
    private TextView bangCD;
    private TextView descBang;
    private String name, bang, imageCD, infoCd;
    private RecyclerView recycleCD;
    ArrayList<Song> songs = new ArrayList<>();
    private ConstraintLayout cdAtivity;




    private void getData(){
        if(getIntent().hasExtra("type") && getIntent().hasExtra("nickname")){
            name = getIntent().getStringExtra("nameCD");
            bang = getIntent().getStringExtra("bangCD");
            infoCd = getIntent().getStringExtra("infoCD");
            imageCD = getIntent().getStringExtra("imageCD");
            songs = (ArrayList<Song>) getIntent().getSerializableExtra("songs");
        } else{
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
        }
    }

    private void setData(){
        nameCD.setText(name);
        bangCD.setText(bang);
        descBang.setText(infoCd);
        Picasso.get().load(imageCD)
                .fit()
                .centerCrop()
                .into(imgCD);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cd);


        // Hook
        nameCD = findViewById(R.id.nameCdInfo);
        bangCD = findViewById(R.id.nameBandInfo);
        descBang = findViewById(R.id.descBang);
        imgCD = findViewById(R.id.imgCDInfo);
        cdAtivity = findViewById(R.id.cdAtivity);
        recycleCD = findViewById(R.id.recycleCD);

        cdAtivity.setBackgroundColor(Color.BLACK);


        recycleCD.setAdapter( new AdpaterSong(songs, this));
        recycleCD.setLayoutManager(new LinearLayoutManager(this));

        getData();
        setData();
    }
}