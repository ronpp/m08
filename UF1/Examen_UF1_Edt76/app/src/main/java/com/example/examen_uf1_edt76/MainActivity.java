package com.example.examen_uf1_edt76;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerH;
    private RecyclerView recyclerV;
    ArrayList<CD> cds = new ArrayList<>();
    ArrayList<Song> songs = new ArrayList<>();
    static String lyrics ;
    private ConstraintLayout main;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Hook
        recyclerH = findViewById(R.id.recyclerH);
        recyclerV = findViewById(R.id.recyclerV);
        main = findViewById(R.id.main);
        main.setBackgroundColor(Color.BLACK);

        initData();
        AdapterCD adapterCD = new AdapterCD(cds, this);
        recyclerH.setAdapter(adapterCD);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL, false);
        recyclerH.setLayoutManager(layoutManager);

        AdpaterSong adpaterSong = new AdpaterSong(songs, this);
        recyclerV.setAdapter(adpaterSong);
        recyclerV.setLayoutManager(new LinearLayoutManager(this));

    }



    private  void initData(){
        List<Song> songs1 = new ArrayList<>();
        List<Song> songs2 = new ArrayList<>();
        List<Song> songs3 = new ArrayList<>();
        List<Song> songs4 = new ArrayList<>();
        List<Song> songs5 = new ArrayList<>();
        List<Song> songs6 = new ArrayList<>();

        Song song11 = new Song("Title Song1 CD 1", "Name Band 1","https://joanseculi.com/images/cds/cd01.jpg", (short)2010, lyrics);
        Song song12 = new Song("Title Song2 CD 1", "Name Band 1","https://joanseculi.com/images/cds/cd01.jpg", (short)2010, lyrics);
        Song song13 = new Song("Title Song3 CD 1", "Name Band 1","https://joanseculi.com/images/cds/cd01.jpg", (short)2010, lyrics);
        Song song14 = new Song("Title Song4 CD 1", "Name Band 1","https://joanseculi.com/images/cds/cd01.jpg", (short)2010, lyrics);
        Song song15 = new Song("Title Song5 CD 1", "Name Band 1","https://joanseculi.com/images/cds/cd01.jpg", (short)2010, lyrics);
        Song song16 = new Song("Title Song6 CD 1", "Name Band 1","https://joanseculi.com/images/cds/cd01.jpg", (short)2010, lyrics);
        songs1.add(song11);
        songs1.add(song12);
        songs1.add(song13);
        songs1.add(song14);
        songs1.add(song15);
        songs1.add(song16);
        CD cd1 = new CD("Name CD 1", "https://joanseculi.com/images/cds/cd01.jpg", "Name Band 1",
                "This is a description of the band 1, this constains information about the genre, the members of the band and also it contains the history of the band",
                songs1);

        cds.add(cd1);

        Song song21 = new Song("Title Song1 CD 2", "Name Band 2","https://joanseculi.com/images/cds/cd02.jpg", (short)2011, lyrics);
        Song song22 = new Song("Title Song2 CD 2", "Name Band 2","https://joanseculi.com/images/cds/cd02.jpg", (short)2011, lyrics);
        Song song23 = new Song("Title Song3 CD 2", "Name Band 2","https://joanseculi.com/images/cds/cd02.jpg", (short)2011, lyrics);
        Song song24 = new Song("Title Song4 CD 2", "Name Band 2","https://joanseculi.com/images/cds/cd02.jpg", (short)2011, lyrics);
        Song song25 = new Song("Title Song5 CD 2", "Name Band 2","https://joanseculi.com/images/cds/cd02.jpg", (short)2011, lyrics);
        Song song26 = new Song("Title Song6 CD 2", "Name Band 2","https://joanseculi.com/images/cds/cd02.jpg", (short)2011, lyrics);
        songs2.add(song21);
        songs2.add(song22);
        songs2.add(song23);
        songs2.add(song24);
        songs2.add(song25);
        songs2.add(song26);

        CD cd2 = new CD("Name CD 2", "https://joanseculi.com/images/cds/cd02.jpg", "Name Band 2",
                "This is a description of the band 2, this constains information about the genre, the members of the band and also it contains the history of the band",
                songs2);

        cds.add(cd2);

        Song song31 = new Song("Title Song1 CD 3", "Name Band 3","https://joanseculi.com/images/cds/cd03.jpg", (short)2011, lyrics);
        Song song32 = new Song("Title Song2 CD 3", "Name Band 3","https://joanseculi.com/images/cds/cd03.jpg", (short)2011, lyrics);
        Song song33 = new Song("Title Song3 CD 3", "Name Band 3","https://joanseculi.com/images/cds/cd03.jpg", (short)2011, lyrics);
        Song song34 = new Song("Title Song4 CD 3", "Name Band 3","https://joanseculi.com/images/cds/cd03.jpg", (short)2011, lyrics);
        Song song35 = new Song("Title Song5 CD 3", "Name Band 3","https://joanseculi.com/images/cds/cd03.jpg", (short)2011, lyrics);
        Song song36 = new Song("Title Song6 CD 3", "Name Band 3","https://joanseculi.com/images/cds/cd03.jpg", (short)2011, lyrics);
        songs3.add(song31);
        songs3.add(song32);
        songs3.add(song33);
        songs3.add(song34);
        songs3.add(song35);
        songs3.add(song36);

        CD cd3 = new CD("Name CD 3", "https://joanseculi.com/images/cds/cd03.jpg", "Name Band 3",
                "This is a description of the band 3, this constains information about the genre, the members of the band and also it contains the history of the band",
                songs3);

        cds.add(cd3);

        Song song41 = new Song("Title Song1 CD 4", "Name Band 2","https://joanseculi.com/images/cds/cd04.jpg", (short)2011, lyrics);
        Song song42 = new Song("Title Song2 CD 4", "Name Band 2","https://joanseculi.com/images/cds/cd04.jpg", (short)2011, lyrics);
        Song song43 = new Song("Title Song3 CD 4", "Name Band 2","https://joanseculi.com/images/cds/cd04.jpg", (short)2011, lyrics);
        Song song44 = new Song("Title Song4 CD 4", "Name Band 2","https://joanseculi.com/images/cds/cd04.jpg", (short)2011, lyrics);
        Song song45 = new Song("Title Song5 CD 4", "Name Band 2","https://joanseculi.com/images/cds/cd04.jpg", (short)2011, lyrics);
        Song song46 = new Song("Title Song6 CD 4", "Name Band 2","https://joanseculi.com/images/cds/cd04.jpg", (short)2011, lyrics);
        songs4.add(song41);
        songs4.add(song42);
        songs4.add(song43);
        songs4.add(song44);
        songs4.add(song45);
        songs4.add(song46);

        CD cd4 = new CD("Name CD 4", "https://joanseculi.com/images/cds/cd04.jpg", "Name Band 4",
                "This is a description of the band 4, this constains information about the genre, the members of the band and also it contains the history of the band",
                songs4);

        cds.add(cd4);

        Song song51 = new Song("Title Song1 CD 5", "Name Band 5","https://joanseculi.com/images/cds/cd05.jpg", (short)2011, lyrics);
        Song song52 = new Song("Title Song2 CD 5", "Name Band 5","https://joanseculi.com/images/cds/cd05.jpg", (short)2011, lyrics);
        Song song53 = new Song("Title Song3 CD 5", "Name Band 5","https://joanseculi.com/images/cds/cd05.jpg", (short)2011, lyrics);
        Song song54 = new Song("Title Song4 CD 5", "Name Band 5","https://joanseculi.com/images/cds/cd05.jpg", (short)2011, lyrics);
        Song song55 = new Song("Title Song5 CD 5", "Name Band 5","https://joanseculi.com/images/cds/cd05.jpg", (short)2011, lyrics);
        Song song56 = new Song("Title Song6 CD 5", "Name Band 5","https://joanseculi.com/images/cds/cd05.jpg", (short)2011, lyrics);
        songs5.add(song51);
        songs5.add(song52);
        songs5.add(song53);
        songs5.add(song54);
        songs5.add(song55);
        songs5.add(song56);

        CD cd5 = new CD("Name CD 5", "https://joanseculi.com/images/cds/cd05.jpg", "Name Band 5",
                "This is a description of the band 5, this constains information about the genre, the members of the band and also it contains the history of the band",
                songs5);

        cds.add(cd5);

        Song song61 = new Song("Title Song1 CD 6", "Name Band 6","https://joanseculi.com/images/cds/cd06.jpg", (short)2011, lyrics);
        Song song62 = new Song("Title Song2 CD 6", "Name Band 6","https://joanseculi.com/images/cds/cd06.jpg", (short)2011, lyrics);
        Song song63 = new Song("Title Song3 CD 6", "Name Band 5","https://joanseculi.com/images/cds/cd05.jpg", (short)2011, lyrics);
        Song song64 = new Song("Title Song4 CD 6", "Name Band 5","https://joanseculi.com/images/cds/cd05.jpg", (short)2011, lyrics);
        Song song65 = new Song("Title Song5 CD 6", "Name Band 6","https://joanseculi.com/images/cds/cd06.jpg", (short)2011, lyrics);
        Song song66 = new Song("Title Song6 CD 6", "Name Band 6","https://joanseculi.com/images/cds/cd06.jpg", (short)2011, lyrics);
        songs6.add(song61);
        songs6.add(song62);
        songs6.add(song63);
        songs6.add(song64);
        songs6.add(song65);
        songs6.add(song66);

        CD cd6 = new CD("Name CD 6", "https://joanseculi.com/images/cds/cd06.jpg", "Name Band 6",
                "This is a description of the band 6, this constains information about the genre, the members of the band and also it contains the history of the band",
                songs6);

        cds.add(cd6);

        songs.add(song11);
        songs.add(song21);
        songs.add(song31);
        songs.add(song41);
        songs.add(song51);
        songs.add(song61);

        songs.add(song12);
        songs.add(song22);
        songs.add(song32);
        songs.add(song42);
        songs.add(song52);
        songs.add(song62);

        songs.add(song13);
        songs.add(song23);
        songs.add(song33);
        songs.add(song43);
        songs.add(song53);
        songs.add(song63);

        songs.add(song14);
        songs.add(song24);
        songs.add(song34);
        songs.add(song44);
        songs.add(song54);
        songs.add(song64);

        songs.add(song15);
        songs.add(song25);
        songs.add(song35);
        songs.add(song45);
        songs.add(song55);
        songs.add(song65);

        songs.add(song16);
        songs.add(song26);
        songs.add(song36);
        songs.add(song46);
        songs.add(song56);
        songs.add(song66);


    }


}