package com.example.examen_uf1_edt76;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AdapterCD extends RecyclerView.Adapter<AdapterCD.MyViewHolder>{


    private ArrayList<CD> cds;
    Context context;

    public AdapterCD(ArrayList<CD> cds, Context context) {
        this.cds = cds;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.row_data_horizontal, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Picasso.get().load(cds.get(position).getImageCD())
                .fit()
                .centerCrop()
                .into(holder.imageCD);
        holder.nameCD.setText(cds.get(position).getNameCD());
        holder.rowLayoutH.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int myPosition = holder.getAdapterPosition();
                Intent intent = new Intent(context, CdActivity.class);
                intent.putExtra("imageCD", cds.get(myPosition).getImageCD());
                intent.putExtra("nameCD", cds.get(myPosition).getNameCD());
                intent.putExtra("bangCD", cds.get(myPosition).getBandCD());
                intent.putExtra("infoCD", cds.get(myPosition).getInfoCD());
                intent.putExtra("songs", (Serializable) cds.get(myPosition).getSongs());
               //TODO: songs
                context.startActivity(intent);

            }
        });


    }

    @Override
    public int getItemCount() {
        return cds.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageCD;
        private TextView nameCD;
        ConstraintLayout rowLayoutH;



        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageCD = itemView.findViewById(R.id.imageCD);
            nameCD = itemView.findViewById(R.id.nameCD);
            rowLayoutH = itemView.findViewById(R.id.rowLayoutH);
        }
    }



}
