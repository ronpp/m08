package com.example.examen_uf1_edt76;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;

public class AdpaterSong  extends RecyclerView.Adapter<AdpaterSong.MyViewHolder>{

    private ArrayList<Song>  songs;
    Context context;

    public AdpaterSong(ArrayList<Song> songs, Context context) {
        this.songs = songs;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.row_data_vertical, parent, false);
        return new AdpaterSong.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Picasso.get().load(songs.get(position).getImageSong())
                .fit()
                .centerCrop()
                .into(holder.imageSong);
        holder.nameSong.setText(songs.get(position).getNameSong());
        holder.bangSong.setText(songs.get(position).getBandSong());
        holder.rowLayoutV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int myPosition = holder.getAdapterPosition();
                Intent intent = new Intent(context, CdActivity.class);
                intent.putExtra("imageSong", songs.get(myPosition).getImageSong());
                intent.putExtra("title", songs.get(myPosition).getNameSong());
                intent.putExtra("songBang", songs.get(myPosition).getBandSong());
                intent.putExtra("year", songs.get(myPosition).getYearSong());
                intent.putExtra("lyric", songs.get(myPosition).getLyrics());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return songs.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageSong;
        private TextView nameSong;
        private TextView bangSong;
        ConstraintLayout rowLayoutV;



        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageSong = itemView.findViewById(R.id.imageSong);
            nameSong = itemView.findViewById(R.id.nameSong);
            bangSong = itemView.findViewById(R.id.bandSong);
            rowLayoutV = itemView.findViewById(R.id.rowLayoutV);
        }
    }

}
