package com.example.edt34_listview;

public class Park {

    private int image;
    private String refugeName;
    private String parkName;
    private String place;
    private String community;
    private String distance;
    private String unevenness;

    private String meters;
    private int num;
    private boolean isOpen;

    public Park(int image, String refugeName, String parkName, String place, String community, String distance, String unevenness, String meters, int num, boolean isOpen) {
        this.image = image;
        this.refugeName = refugeName;
        this.parkName = parkName;
        this.place = place;
        this.community = community;
        this.distance = distance;
        this.unevenness = unevenness;
        this.meters = meters;
        this.num = num;
        this.isOpen = isOpen;
    }

    public int getImage() {
        return image;
    }

    public String getRefugeName() {
        return refugeName;
    }

    public String getParkName() {
        return parkName;
    }

    public String getPlace() {
        return place;
    }

    public String getCommunity() {
        return community;
    }

    public String getDistance() {
        return distance;
    }

    public String getUnevenness() {
        return unevenness;
    }

    public String getMeters() {
        return meters;
    }

    public int getNum() {
        return num;
    }

    public boolean isOpen() {
        return isOpen;
    }
}
