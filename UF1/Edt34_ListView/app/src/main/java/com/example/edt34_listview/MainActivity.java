package com.example.edt34_listview;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Pair;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private ImageView imgMainBack;
    private ImageView imgLogoApp;
    private LinearLayout containerMainText;
    private TextView txtMainTitle;


    private void splashAnimation(ImageView imgApp, TextView txtApp){
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(MainActivity.this, DashboardActivity.class);
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(
                        MainActivity.this,
                        Pair.create(imgApp, imgApp.getTransitionName()),
                        Pair.create(txtApp, txtApp.getTransitionName())
                );
                startActivity(intent, options.toBundle());
            }
        }, 3000);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hook
        imgMainBack = findViewById(R.id.imgMainBack);
        imgLogoApp = findViewById(R.id.imgLogoApp);
        containerMainText = findViewById(R.id.containerTextMain);
        txtMainTitle = findViewById(R.id.txtMainTitle);

        // Hiden
        imgMainBack.setAlpha(0f);
        imgLogoApp.setTranslationX(-1000f);
        containerMainText.setTranslationX(1000f);

        // Animations
        imgMainBack.animate().alpha(1).setStartDelay(1500).setDuration(1000);
        imgLogoApp.animate().translationX(0f).setDuration(1000).setStartDelay(500);
        containerMainText.animate().translationX(-0f).setDuration(1000).setStartDelay(500);

        splashAnimation(imgLogoApp, txtMainTitle);
    }
}