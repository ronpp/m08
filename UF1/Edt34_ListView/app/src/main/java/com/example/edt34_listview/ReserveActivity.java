package com.example.edt34_listview;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class ReserveActivity extends AppCompatActivity {

    public static final String PREF_FILE_NAME = "SharedDataFile";

    private Spinner spnParks;
    private Switch swPension;
    private RadioGroup rdgPension;
    private RadioButton rdbNone;
    private TextView dateEntry;
    private TextView dateExit;
    private ImageView imgReserva;
    private Button btnReserve;
    private TextInputEditText txtName;
    private TextInputEditText txtLastname;
    private TextInputEditText txtEmail;
    private TextInputEditText txtCountPerson;



    private void saveData(){
        SharedPreferences sharedPreferences = getSharedPreferences(PREF_FILE_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("name", txtName.getText().toString());
        editor.putString("lastname", txtLastname.getText().toString());
        editor.putString("email", txtEmail.getText().toString());
        editor.putString("countPerson", txtCountPerson.getText().toString());
        String isPensionChecked = swPension.isChecked()? "true":"false";
        editor.putString("pension", isPensionChecked);
        if(swPension.isChecked()){
            int selectRadioButtonId = rdgPension.getCheckedRadioButtonId();
            editor.putInt("pensionValue", selectRadioButtonId);
        }
        editor.apply();
        Toast.makeText(ReserveActivity.this, "Data Saved", Toast.LENGTH_SHORT).show();
    }

    private void loadData(){
        SharedPreferences sharedPreferences = getSharedPreferences(PREF_FILE_NAME,
                Context.MODE_PRIVATE);
        String name = sharedPreferences.getString("name", "No data found");
        String email = sharedPreferences.getString("email", "No data found");
        String lastname = sharedPreferences.getString("lastname", "No data found");
        String countPerson = sharedPreferences.getString("countPerson", "No data found");
        String isPension = sharedPreferences.getString("pension", "");
        txtName.setText(name);
        txtEmail.setText(email);
        txtLastname.setText(lastname);
        txtCountPerson.setText(countPerson);

        if("true".equals(isPension)){
            swPension.setChecked(true);
            int selectRadioButtonId = sharedPreferences.getInt("pensionValue", 0);
            RadioButton rd = findViewById(selectRadioButtonId);
            rd.setChecked(true);
        }
        Toast.makeText(ReserveActivity.this, "Data Load", Toast.LENGTH_SHORT).show();

    }

    private void enableRadioGroup(RadioGroup radioGroup, boolean isEnable){
        for(int i = 0; i < radioGroup.getChildCount(); i++){
            radioGroup.getChildAt(i).setClickable(isEnable);
        }
    }

    private DatePickerDialog datePicker(TextView txtDate){
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        DatePickerDialog picker = new DatePickerDialog(ReserveActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        // Show selected date
                        month ++; // Cause month is 0-11
                        txtDate.setText(day + "/" + month + "/" + year);
                    }
                }, year, month, day);
        return picker;
    }

    private void sendMail(String[] TO, String[] CC, String subject, String msg){
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, msg);
        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        }catch (android.content.ActivityNotFoundException ex){
            Toast.makeText(ReserveActivity.this, "There not email client installed",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public String getRadioButtonChecked(RadioGroup radioGroup){
        int selectRadioButtonId = radioGroup.getCheckedRadioButtonId();
        RadioButton radioButton = findViewById(selectRadioButtonId);
        return radioButton.getText().toString();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reserve);

        // Hook
        spnParks = findViewById(R.id.spnParks);
        swPension = findViewById(R.id.swPension);
        rdgPension = findViewById(R.id.rdgPension);
        rdbNone = findViewById(R.id.rdbNone);
        dateEntry = findViewById(R.id.dateEntry);
        dateExit = findViewById(R.id.dateExit);
        imgReserva = findViewById(R.id.imgReserva);
        btnReserve = findViewById(R.id.btnReserve);
        txtName = findViewById(R.id.txtName);
        txtLastname = findViewById(R.id.txtLastname);
        txtEmail= findViewById(R.id.txtEmail);
        txtCountPerson= findViewById(R.id.txtCountPerson);


        // Load Data from local store
        loadData();

        //  Spinner Data
        List<String> parkList = Arrays.asList(
                "Selecciona Refugi",
                "Refugi Josep Maria Blanc",
                "Refugi Cap de Llauset",
                "Refugi Ventosa i Clavell",
                "Refugi Amitges",
                "Refugi Josep Maria Montfort" );
        ArrayAdapter<String>dataAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_activated_1,
                parkList );
        spnParks.setAdapter(dataAdapter);

        // List of Image resource
        List<Integer> imgResource = Arrays.asList(
                0, //
                R.drawable.foto1,
                R.drawable.foto2,
                R.drawable.foto3,
                R.drawable.foto4,
                R.drawable.foto5 );


        // Disable radio group
        enableRadioGroup(rdgPension, false);

        // Get data intent from Dashboard Activity and set in Reserve Activity
        Intent intent = getIntent();
        imgReserva.setImageResource(intent.getIntExtra(DashboardActivity.PARK_IMAGE, 0));
        String refuge_name = intent.getStringExtra(DashboardActivity.PARK_REFUGE);
        int index = parkList.indexOf(refuge_name);
        spnParks.setSelection(index);


        // Listeners
        spnParks.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                imgReserva.setImageResource(imgResource.get(position));
                String selectedItem = parent.getItemAtPosition(position).toString();
                Toast.makeText(ReserveActivity.this, selectedItem, Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        swPension.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    enableRadioGroup(rdgPension, isChecked);
                    Toast.makeText(ReserveActivity.this, "Switch: Active", Toast.LENGTH_SHORT).show();
                } else {
                    enableRadioGroup(rdgPension, isChecked);
                    Toast.makeText(ReserveActivity.this, "Switch: Disable", Toast.LENGTH_SHORT).show();
                    rdbNone.setChecked(true);
                }
            }
        });

        rdgPension.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                String selected = ( (RadioButton)findViewById(checkedId)).getText().toString();
                Toast.makeText(ReserveActivity.this, selected, Toast.LENGTH_SHORT).show();
            }
        });

        dateEntry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePicker(dateEntry).show();
            }
        });

        dateExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePicker(dateExit).show();
            }
        });



//        btnReserve.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//            String[] TO = {txtEmail.getText().toString()};
//            String[] CC = {txtEmail.getText().toString()};
//            String subject = "Reserva per APP";
//            String msg = "Nom: " + txtName.getText() +
//                    "\nRefugi: " + spnParks.getSelectedItem() +
//                    "\nEmail: " + txtEmail.getText() +
//                    "\nNum Persones: " + txtCountPerson.getText() +
//                    "\nPensio Completa: " + getRadioButtonChecked(rdgPension) +
//                    "\nData Entrada: " + dateEntry.getText()+
//                    "\nData Sortida: " + dateExit.getText();
//
//            sendMail(TO, CC, subject,msg);
//            }
//        });
        btnReserve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveData();
            }
        });
    }
}