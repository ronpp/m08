package com.example.edt34_listview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

public class DashboardActivity extends AppCompatActivity {

    public static String PARK_IMAGE = "com.example.edt34_listview.PARK_IMAGE";
    public static String PARK_REFUGE = "com.example.edt34_listview.PARK_REFUGE";

    private ListView listPark;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        // Hook
        listPark = findViewById(R.id.listPark);

        // Parks
        Park park1 = new Park(R.drawable.foto1, "Refugi Josep Maria Blanc",
                                "Parc Aigüestortes", "Vall d'Aran",
                                "Catalunya", "2:30h",
                                "1200m", "2.100m", 30 , true);

        Park park2 = new Park(R.drawable.foto2, "Refugi Cap de Llauset",
                "Posets-Maladeta", "Osca ", "Arago",
                "2:15h", "1100m", "2.800m", 25, true);

        Park park3 = new Park(R.drawable.foto3, "Refugi Ventosa i Clavell",
                "Parc Aigüestortes", "Vall d'Aran", "Catalunya",
                "3:15h", "800m", "2.150m", 45, false);

        Park park4 = new Park(R.drawable.foto4, "Refugi Amitges",
                "Parc Aigüestortes", "Vall d'Aran", "Catalunya",
                "2:30h", "750m", "2.400m", 87, true);

        Park park5 = new Park(R.drawable.foto5, "Refugi Josep Maria Montfort",
                "Alt Pirineu", "Vall Ferrera", "Catalunya",
                "2:30h", "950m", "1.875m", 23, true);


        List<Park> parks = Arrays.asList(park1, park2, park3, park4, park5);
        CustomAdapter customApdapter = new CustomAdapter(this, parks);
        listPark.setAdapter(customApdapter);

        listPark.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(DashboardActivity.this, ReserveActivity.class);
                int park_image = parks.get(position).getImage();
                String park_refuge = parks.get(position).getRefugeName();
                intent.putExtra(PARK_IMAGE, park_image);
                intent.putExtra(PARK_REFUGE,park_refuge);
                startActivity(intent);
            }
        });

    }

    private class CustomAdapter extends BaseAdapter {

        private Context context;
        private List<Park> parks; //data source of the list adapter

        public CustomAdapter(Context context, List<Park> parks) {
            this.context = context;
            this.parks = parks;
        }

        @Override
        public int getCount() {
            return parks.size();
        }

        @Override
        public Object getItem(int position) {
            return parks.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.row_data2, null);
            ImageView imgPark = view.findViewById(R.id.imgPark2);
            TextView txtRefugeName = view.findViewById(R.id.txtRefugeName2);
            TextView txtParkName = view.findViewById(R.id.txtParkName2);
            TextView txtPlace = view.findViewById(R.id.txtLocation);
            TextView txtCommunity = view.findViewById(R.id.txtCommunity2);
            TextView txtDistance = view.findViewById(R.id.txtDistance2);
            TextView txtUnevenness = view.findViewById(R.id.txtUnevenness2);
            TextView txtMeter = view.findViewById(R.id.txtMeter2);
            TextView txtNum = view.findViewById(R.id.txtNum);
            TextView txtOpen = view.findViewById(R.id.txtOpen);



            imgPark.setImageResource(parks.get(position).getImage());
            txtRefugeName.setText(parks.get(position).getRefugeName());
            txtParkName.setText(parks.get(position).getParkName());
            txtPlace.setText(parks.get(position).getPlace());
            txtCommunity.setText(parks.get(position).getCommunity());
            txtDistance.setText(parks.get(position).getDistance());
            txtUnevenness.setText(parks.get(position).getUnevenness());
            txtMeter.setText(parks.get(position).getMeters());
            txtNum.setText(parks.get(position).getNum()+"");

            boolean isOpen = parks.get(position).isOpen();
            txtOpen.setText(isOpen ? "Open": "Close");
            if ("Close".equals(txtOpen.getText())){
                txtOpen.setTextColor(Color.RED);
            }
            return view;
        }
    }
}