package com.example.splashscreenanimation;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.Calendar;

public class SignupActivity extends AppCompatActivity {

    private TextView txtAccount;
    private Spinner spnBikes;
    private ArrayList<Bike> bikes;
    private BikeAdapter bikeAdapter;
    private LinearLayout containerSignUp;

    DatePickerDialog picker;
    TextInputEditText txtBirthdate;


    public void animateText(TextView textView){
        ObjectAnimator objAnimator = ObjectAnimator.ofFloat(textView, "scaleY", 0.8f, 1f);
        objAnimator.setDuration(500);
        ObjectAnimator objAnimator2 = ObjectAnimator.ofFloat(textView, "scaleX", 0.8f, 1f);
        objAnimator2.setDuration(500);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objAnimator, objAnimator2);
        animatorSet.start();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        // Hook
        txtAccount = findViewById(R.id.txtAccount);
        txtBirthdate = findViewById(R.id.txtBirthdate);
        containerSignUp = findViewById(R.id.containerSignUp);
        spnBikes = findViewById(R.id.spnBikes);

        // Animations
        containerSignUp.setScaleX(0f);
        containerSignUp.setScaleY(0f);
        containerSignUp.animate().scaleX(1).scaleY(1).setDuration(1000);

        // Listeners
        txtAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animateText(txtAccount);
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(SignupActivity.this, DashboardActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.animator.slide_in_right, R.animator.slide_out_left);
                    }
                }, 500);
            }
        });

        txtBirthdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                picker = new DatePickerDialog(SignupActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                                // Show selected date
                                month ++; // Cause month is 0-11
                                txtBirthdate.setText(day + "/" + month + "/" + year);
                            }
                        }, year, month, day);
                picker.show();
            }
        });

        // Spinner Bikes
        initList();
        bikeAdapter = new BikeAdapter(this, bikes);
        spnBikes.setAdapter(bikeAdapter);

    }

    private void initList() {
        bikes = new ArrayList<>();
        bikes.add(new Bike("Mountain Bike", R.drawable.bike01));
        bikes.add(new Bike("Cycling Bike", R.drawable.bike02));
        bikes.add(new Bike("Folding Bike", R.drawable.bike03));
        bikes.add(new Bike("Hybrid Bike", R.drawable.bike04));
        bikes.add(new Bike("Electric Bike", R.drawable.bike06));
        bikes.add(new Bike("Road Bike", R.drawable.bike08));
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.animator.slide_in_left, R.animator.slide_out_right);
    }
}