package com.example.splashscreenanimation;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Pair;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public static  int SPLASH_SCREEN = 3000;
    private ImageView imgMainApp;
    private TextView txtMainApp;
    private TextView txtMainSubtitle;


    private void splashAnimation(ImageView imgApp, TextView txtApp){
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(MainActivity.this, DashboardActivity.class);
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(
                        MainActivity.this,
                        Pair.create(imgApp, imgApp.getTransitionName()),
                        Pair.create(txtApp, txtApp.getTransitionName())
                );
                startActivity(intent, options.toBundle());
            }
        }, SPLASH_SCREEN);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hook
        imgMainApp = findViewById(R.id.imgMainApp);
        txtMainApp = findViewById(R.id.txtMainApp);
        txtMainSubtitle = findViewById(R.id.txtMainSubtitle);

        // Hiden
        imgMainApp.setTranslationY(-1000f);
        txtMainApp.setTranslationY(900f);
        txtMainApp.setRotation(180f);
        txtMainSubtitle.setRotation(90f);
        txtMainSubtitle.setTranslationY(900f);;

        // Animations
        imgMainApp.animate().translationY(100f).rotation(360f).setDuration(1000).setStartDelay(500);
        txtMainApp.animate().translationY(100f).rotation(360f).setDuration(1000).setStartDelay(600);
        txtMainSubtitle.animate().translationY(100f).rotation(-0f).setDuration(1000).setStartDelay(700);
        splashAnimation(imgMainApp, txtMainApp);
    }
}