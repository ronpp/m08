package com.example.splashscreenanimation;

public class Bike {

    private String bikeType;
    private  int imgBike;

    public Bike(String bikeType, int imgBike){
        this.bikeType = bikeType;
        this.imgBike = imgBike;
    }

    public String getBikeType() {return bikeType;}
    public void setBikeType(String bikeType) {this.bikeType = bikeType;}
    public int getImgBike() {return imgBike;}
    public void setImgBike(int imgBike) {this.imgBike = imgBike;}
}
