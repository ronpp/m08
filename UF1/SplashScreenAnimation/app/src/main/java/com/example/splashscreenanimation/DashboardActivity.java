package com.example.splashscreenanimation;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DashboardActivity extends AppCompatActivity {

    private TextView txtNewUser;
    private TextView txtForgot;
    private LinearLayout containerLogin;



    public void gotActivity(Intent intents){
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = intents;
                startActivity(intent);
                overridePendingTransition(R.animator.slide_in_right, R.animator.slide_out_left);
            }
        }, 500);
    }

    public void animateText(TextView textView){
        ObjectAnimator objAnimator = ObjectAnimator.ofFloat(textView, "scaleY", 0.8f, 1f);
        objAnimator.setDuration(500);
        ObjectAnimator objAnimator2 = ObjectAnimator.ofFloat(textView, "scaleX", 0.8f, 1f);
        objAnimator2.setDuration(500);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objAnimator, objAnimator2);
        animatorSet.start();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        // Hook
        txtNewUser = findViewById(R.id.txtNewUser);
        txtForgot = findViewById(R.id.txtForgot);
        containerLogin = findViewById(R.id.containerLogin);

        // Animations
        containerLogin.setScaleX(0f);
        containerLogin.setScaleY(0f);
        containerLogin.animate().scaleX(1).scaleY(1).setDuration(1000);

        // Listeners
        txtNewUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animateText(txtNewUser);
                gotActivity(new Intent(DashboardActivity.this, SignupActivity.class));
            }
        });

        txtForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animateText(txtForgot);
              gotActivity(new Intent(DashboardActivity.this, ForgotpasswordActivity.class));
            }
        });

    }
}