package com.example.splashscreenanimation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class BikeAdapter extends ArrayAdapter<Bike> {

    // Constructor
    public BikeAdapter(@NonNull Context context, ArrayList<Bike> bikesArrayList) {
        super(context, 0, bikesArrayList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return  init(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return  init(position, convertView, parent);
    }



    public View init(int position, @Nullable View convertView, @NonNull ViewGroup parent){
        //layout
        if (convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.spinner_row,
                    parent,
                    false
            );
        }
        // Hook
        ImageView imgBike = convertView.findViewById(R.id.imgBike);
        TextView txtBike = convertView.findViewById(R.id.txtBike);

        // setText and setImageResource
        Bike currentItem = getItem(position);

        if (currentItem != null){
            imgBike.setImageResource(currentItem.getImgBike());
            txtBike.setText(currentItem.getBikeType());
        }
        return convertView;
    }

}
