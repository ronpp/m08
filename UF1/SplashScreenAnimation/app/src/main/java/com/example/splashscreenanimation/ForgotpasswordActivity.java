package com.example.splashscreenanimation;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ForgotpasswordActivity extends AppCompatActivity {

    private TextView txtNewUserForgot;
    private LinearLayout containerForgot;


    public void animateText(TextView textView){
        ObjectAnimator objAnimator = ObjectAnimator.ofFloat(textView, "scaleY", 0.8f, 1f);
        objAnimator.setDuration(500);
        ObjectAnimator objAnimator2 = ObjectAnimator.ofFloat(textView, "scaleX", 0.8f, 1f);
        objAnimator2.setDuration(500);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objAnimator, objAnimator2);
        animatorSet.start();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotpassword);

        // Hook
        txtNewUserForgot = findViewById(R.id.txtNewUserForgot);
        containerForgot = findViewById(R.id.containerForgot);

        // Animations
        containerForgot.setScaleX(0f);
        containerForgot.setScaleY(0f);
        containerForgot.animate().scaleX(1).scaleY(1).setDuration(1000);

        // Listeners
        txtNewUserForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animateText(txtNewUserForgot);
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(ForgotpasswordActivity.this, SignupActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.animator.slide_in_right, R.animator.slide_out_left);
                    }
                }, 500);
            }
        });
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.animator.slide_in_left, R.animator.slide_out_right);
    }
}