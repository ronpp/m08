package com.example.actcalculator;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

public class MainActivity extends AppCompatActivity {

    private static  int SPLASH_SCREEN = 500;
    private Button btnAdd;
    private Button btnSubst;
    private Button btnMult;
    private Button btnDiv;
    private Button btnPower;
    private Button btnCE;
    private TextInputEditText txtResult;
    private TextInputEditText txtNumber1;
    private TextInputEditText txtNumber2;



    public void aboutUs(View view) {
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(MainActivity.this, activity_aboutus.class);
                startActivity(intent);
            }
        }, SPLASH_SCREEN);
    }

    public AlertDialog setAlert(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setTitle("Warning!");
        builder.setPositiveButton("Accept", null);
        return builder.create();
    }

    public String getButtonValue( TextInputEditText input){
        return input.getText().toString().trim();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hooks
        txtResult = findViewById(R.id.txtResult);
        txtNumber1 =findViewById(R.id.txtNumber1);
        txtNumber2 =findViewById(R.id.txtNumber2);
        btnAdd = findViewById(R.id.btnAdd);
        btnSubst = findViewById(R.id.btnSubst);
        btnMult = findViewById(R.id.btnMult);
        btnDiv = findViewById(R.id.btnDiv);
        btnPower = findViewById(R.id.btnPower);
        btnCE = findViewById(R.id.btnCE);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double result;
                if(getButtonValue(txtNumber1).isEmpty() || getButtonValue(txtNumber2).isEmpty()){
                    setAlert("You need enter a number").show();
                }else{
                    result = Calculator.add(Double.parseDouble(getButtonValue(txtNumber1)),
                                            Double.parseDouble(getButtonValue(txtNumber2)));
                    txtResult.setText(result + "");
                }
            }
        });

        btnSubst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double result;
                if(getButtonValue(txtNumber1).isEmpty() || getButtonValue(txtNumber2).isEmpty()){
                    setAlert("You need enter a number").show();
                }else{
                    result = Calculator.subst(Double.parseDouble(getButtonValue(txtNumber1)),
                            Double.parseDouble(getButtonValue(txtNumber2)));
                    txtResult.setText(result + "");
                }
            }
        });


        btnMult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double result;
                if(getButtonValue(txtNumber1).isEmpty() || getButtonValue(txtNumber2).isEmpty()){
                    setAlert("You need enter a number").show();
                }else{
                    result = Calculator.mult(Double.parseDouble(getButtonValue(txtNumber1)),
                            Double.parseDouble(getButtonValue(txtNumber2)));
                    txtResult.setText(result + "");
                }
            }
        });


        btnDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double result;
                if(getButtonValue(txtNumber1).isEmpty() || getButtonValue(txtNumber2).isEmpty()) {
                    setAlert("You need enter a number").show();
                    
                }else if(Calculator.isZeroValue(getButtonValue(txtNumber1)) ||
                         Calculator.isZeroValue(getButtonValue(txtNumber2))){
                    setAlert("Divide by zero is impossible").show();
                }else{
                    result = Calculator.div(Double.parseDouble(getButtonValue(txtNumber1)),
                            Double.parseDouble(getButtonValue(txtNumber2)));
                    txtResult.setText(result + "");
                }
            }
        });

        btnPower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double result;
                if(getButtonValue(txtNumber1).isEmpty() || getButtonValue(txtNumber2).isEmpty()){
                    setAlert("You need enter a number").show();
                }else{
                    result = Calculator.power(Double.parseDouble(getButtonValue(txtNumber1)),
                            Double.parseDouble(getButtonValue(txtNumber2)));
                    txtResult.setText(result + "");
                }
            }
        });

        btnCE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtResult.setText("0.0");
                txtNumber1.setText("");
                txtNumber2.setText("");
            }
        });



    }
}