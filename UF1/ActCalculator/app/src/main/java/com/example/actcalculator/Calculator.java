package com.example.actcalculator;

import android.widget.Toast;

public class Calculator {


    // validation  methods
    public static boolean isZeroValue(String value){return  Integer.parseInt(value) == 0;}



    // Operation methods
    public static double add(double number1, double number2){ return number1 + number2;}
    public static double subst(double number1, double number2){ return number1 - number2;}
    public static double mult(double number1, double number2){ return number1 * number2;}
    public static double div(double number1, double number2){ return number1 / number2;}
    public static double power(double number1, double number2){ return Math.pow(number1, number2);}



}
