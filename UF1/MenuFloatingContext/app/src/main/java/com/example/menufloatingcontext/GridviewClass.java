package com.example.menufloatingcontext;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;


public class GridviewClass extends AppCompatActivity {

    private GridView gridViewClass;
    private List<Item> items;
    CustomAdapter customAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gridview_class);


        // Hook
        gridViewClass = findViewById(R.id.gridviewClass);


        initData();
        customAdapter = new CustomAdapter(this, items);
        gridViewClass.setAdapter(customAdapter);
        registerForContextMenu(gridViewClass);


    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Choose an options");
        menu.add(0,1 , 1, "Deletes");
        menu.add(0,2 , 2, "Shares");
        menu.add(0,3 , 3, "Webs");
    }


    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                //delete
                return true;
            case 2:
                //send mail
                return true;
            case 3:
                Intent i2 = new Intent(Intent.ACTION_VIEW);
                //share
                return true;
            default:
                return super.onContextItemSelected(item);
        }

    }

    // Initialization data
    private void initData(){

        Item item1 = new Item("Penguin", R.drawable.penguin);
        item1.setDesc("This is the description of the animal " + item1.getItemName());
        Item item2 = new Item("Rhino", R.drawable.rhino);
        item2.setDesc("This is the description of the animal " + item2.getItemName());
        Item item3 = new Item("Panther", R.drawable.panther);
        item3.setDesc("This is the description of the animal " + item3.getItemName());
        Item item4 = new Item("Alligator", R.drawable.alligator);
        item4.setDesc("This is the description of the animal " + item4.getItemName());
        Item item5 = new Item("Wolf", R.drawable.wolf);
        item5.setDesc("This is the description of the animal " + item5.getItemName());
        Item item6 = new Item("Hippo", R.drawable.hippo);
        item6.setDesc("This is the description of the animal " + item6.getItemName());
        Item item7 = new Item("Monkey", R.drawable.monkey);
        item7.setDesc("This is the description of the animal " + item7.getItemName());
        Item item8 = new Item("Bull", R.drawable.bull);
        item8.setDesc("This is the description of the animal " + item8.getItemName());
        Item item9 = new Item("Eagle", R.drawable.eagle);
        item9.setDesc("This is the description of the animal " + item9.getItemName());
        Item item10 = new Item("Penguin", R.drawable.penguin);
        item10.setDesc("This is the description of the animal " + item10.getItemName());
        Item item11 = new Item("Rhino", R.drawable.rhino);
        item11.setDesc("This is the description of the animal " + item11.getItemName());
        Item item12 = new Item("Panther", R.drawable.panther);
        item12.setDesc("This is the description of the animal " + item12.getItemName());
        Item item13 = new Item("Alligator", R.drawable.alligator);
        item13.setDesc("This is the description of the animal " + item13.getItemName());
        Item item14 = new Item("Wolf", R.drawable.wolf);
        item14.setDesc("This is the description of the animal " + item14.getItemName());
        Item item15 = new Item("Hippo", R.drawable.hippo);
        item15.setDesc("This is the description of the animal " + item15.getItemName());
        Item item16 = new Item("Monkey", R.drawable.monkey);
        item16.setDesc("This is the description of the animal " + item16.getItemName());
        Item item17 = new Item("Bull", R.drawable.bull);
        item16.setDesc("This is the description of the animal " + item16.getItemName());
        Item item18 = new Item("Eagle", R.drawable.eagle);
        item18.setDesc("This is the description of the animal " + item18.getItemName());
        items = Arrays.asList(item18, item17, item16, item15, item14, item13, item12, item11, item10,
                item9, item8, item7, item6, item5, item4, item3, item2, item1);
    }

    // Inner Class
    private class CustomAdapter extends BaseAdapter {


        private Context context;
        private List<Item> items;

        public CustomAdapter(Context context, List<Item> items) {
            this.context = context;
            this.items = items;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.row_data_grid, null);
            TextView textRow = view.findViewById(R.id.txtRowGrid);
            ImageView imageRow =  view.findViewById(R.id.imageRowGrid);
            RelativeLayout rowLauout = view.findViewById(R.id.rowLayout);

            textRow.setText(items.get(position).getItemName());
            imageRow.setImageResource(items.get(position).getImage());

            rowLauout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, DetailActivity.class);
                    intent.putExtra("title", items.get(position).getItemName());
                    intent.putExtra("img", items.get(position).getImage());
                    intent.putExtra("desc", items.get(position).getDesc());
                    context.startActivity(intent);
                }
            });

            return view;
        }
    }


}


