package com.example.menufloatingcontext;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button btnListText;
    private Button btnGridText;
    private Button btnListImg;
    private Button btnGridImg;
    private Button btnGridClass;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Hook
        btnListText = findViewById(R.id.btnListText);
        btnGridText = findViewById(R.id.btnGridText);
        btnListImg = findViewById(R.id.btnListImg);
        btnGridImg = findViewById(R.id.btnGridImg);
        btnGridClass = findViewById(R.id.btnGridClass);

        btnListText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ListviewFloatingMenu.class);
                startActivity(intent);
            }
        });

        btnGridText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, GridviewText.class);
                startActivity(intent);
            }
        });


        btnListImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ListviewImages.class);
                startActivity(intent);
            }
        });

        btnGridImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, GridviewImages.class);
                startActivity(intent);
            }
        });

        btnGridClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent intent = new Intent(MainActivity.this, GridviewClass.class);
                startActivity(intent);
            }
        });

    }


}