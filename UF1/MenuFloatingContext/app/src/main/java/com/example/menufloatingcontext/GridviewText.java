package com.example.menufloatingcontext;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

public class GridviewText extends AppCompatActivity {

    private GridView gridView;
    List<String> cursos = new ArrayList<>();
    ArrayAdapter<String> dataAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gridview_text);

        // Hook
        gridView = findViewById(R.id.gridviewText);

        cursos.add("1JSIM");
        cursos.add("2JSIM");
        cursos.add("1HISX");
        cursos.add("2HISX");
        cursos.add("1HISM");
        cursos.add("2HISM");
        cursos.add("1HIAW");
        cursos.add("2HIAW");
        cursos.add("1JSIM");
        cursos.add("2JSIM");
        cursos.add("1HISX");
        cursos.add("2HISX");
        cursos.add("1HISM");
        cursos.add("2HISM");
        cursos.add("1HIAW");
        cursos.add("2HIAW");

        dataAdapter = new ArrayAdapter<String>(this,
                R.layout.support_simple_spinner_dropdown_item,
                cursos);
        gridView.setAdapter(dataAdapter);
        registerForContextMenu(gridView);
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Choose an options");
        menu.add(0,1 , 1, "Deletes");
        menu.add(0,2 , 2, "Shares");
        menu.add(0,3 , 3, "Webs");
    }


    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        switch (item.getItemId()) {
            case 1:
                //delete
                return true;
            case 2:
                //send mail
                return true;
            case 3:
                Intent i2 = new Intent(Intent.ACTION_VIEW);
                //share
                return true;
            default:
                return super.onContextItemSelected(item);
        }

    }
}