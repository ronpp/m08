package com.example.menufloatingcontext;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    private TextView textTitle;
    private ImageView imgDetails;
    private  TextView textDesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        // Hook

        textTitle = findViewById(R.id.textTitle);
        imgDetails = findViewById(R.id.imgDetail);
        textDesc = findViewById(R.id.textDesc);

        Intent intent = getIntent();
        textTitle.setText(intent.getStringExtra("title"));
        imgDetails.setImageResource(intent.getIntExtra("img", 0));
        textDesc.setText(intent.getStringExtra("desc"));
    }
}