package com.example.menufloatingcontext;

public class Item {

    private  String itemName;
    private int image;
    private String desc;

    public Item(String itemName, int image) {
        this.itemName = itemName;
        this.image = image;
    }


    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
