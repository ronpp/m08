package com.example.menufloatingcontext;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ListviewImages extends AppCompatActivity {

    private ListView listViewImg;
    private Integer[] imagesID = {
            R.drawable.penguin,
            R.drawable.rhino,
            R.drawable.panther,
            R.drawable.alligator,
            R.drawable.wolf,
            R.drawable.hippo,
            R.drawable.monkey,
            R.drawable.bull,
            R.drawable.eagle,
            R.drawable.penguin,
            R.drawable.rhino,
            R.drawable.panther,
            R.drawable.alligator,
            R.drawable.wolf,
            R.drawable.hippo,
            R.drawable.monkey,
            R.drawable.bull,
            R.drawable.eagle,
    };

    CustomAdapter customAdapter = new CustomAdapter();

    private String[] imagesText = {
            "Penguin", "Rhino", "Panther", "Alligator", "Wolf", "Hippo", "Monkey", "Bull", "Eagle",
            "Penguin", "Rhino", "Panther", "Alligator", "Wolf", "Hippo", "Monkey", "Bull", "Eagle"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview_images);

        // Hook
        listViewImg = findViewById(R.id.listviewImg);

        listViewImg.setAdapter(customAdapter);
        registerForContextMenu(listViewImg);
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Choose an options");
        menu.add(0,1 , 1, "Deletes");
        menu.add(0,2 , 2, "Shares");
        menu.add(0,3 , 3, "Webs");
    }


    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                //delete
                return true;
            case 2:
                //send mail
                return true;
            case 3:
                Intent i2 = new Intent(Intent.ACTION_VIEW);
                //share
                return true;
            default:
                return super.onContextItemSelected(item);
        }

    }

    // Inner Class
    private class CustomAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return imagesID.length;
        }

        @Override
        public Object getItem(int position) {
            return imagesID[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.row_data_list, null);
            TextView textRow = view.findViewById(R.id.txtRowList);
            ImageView imageRow =  view.findViewById(R.id.imageRowList);

            textRow.setText(imagesText[position]);
            imageRow.setImageResource(imagesID[position]);

            return view;
        }
    }
}